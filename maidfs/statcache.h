#ifndef STATCACHE_H
#define STATCACHE_H

//#define FUSE_USE_VERSION 30

#include <mutex>
#include <string>
#include <unordered_map>
#include <vector>

#include <fuse.h>
#include <sys/stat.h>

#include "backing.h"
#include "cvector.h"
// #include "HashMap.h"
#include "parallel_hashmap/phmap.h"

struct statcache_entry
{
	std::string name;
	struct stat st;
	cvector<struct statcache_entry*> child_dirs;
	cvector<struct statcache_entry*> child_files;
	backing* b;
};

class statcache
{
public:
	statcache();
	
	int add(std::string path, struct stat st);
	int add(std::string path, struct stat st, backing* b);
	int repl(std::string path, struct stat st);
	int repl(std::string path, backing* b);
	int del(std::string path);
	int delr(std::string path);
	int get(std::string path, struct stat &st);
	int getd(std::string path, struct stat &st);
	int getf(std::string path, struct stat &st);
	backing* getbacking(std::string path);
	bool exists(std::string path);
	bool exists(std::string path, char &type);
	int filldir(std::string path, void* buf, fuse_fill_dir_t filler);
	
	virtual ~statcache();
private:
	std::string getParentPath(std::string path, int last_slash_pos = -1);
	std::string addrtopath(struct statcache_entry* e);
	
	phmap::parallel_node_hash_map<
		std::string,
		struct statcache_entry*,
		phmap::priv::hash_default_hash<std::string>,
		phmap::priv::hash_default_eq<std::string>,
		phmap::priv::Allocator<phmap::priv::Pair<const std::string, struct statcache_entry*>>,
		4,
		std::shared_mutex
		> directories;
	phmap::parallel_node_hash_map<
		uintptr_t,
		std::string,
		phmap::priv::hash_default_hash<uintptr_t>,
		phmap::priv::hash_default_eq<uintptr_t>,
		phmap::priv::Allocator<phmap::priv::Pair<uintptr_t, const std::string>>,
		4,
		std::shared_mutex
		> directories_reverse;
	phmap::parallel_node_hash_map<
		std::string,
		struct statcache_entry*,
		phmap::priv::hash_default_hash<std::string>,
		phmap::priv::hash_default_eq<std::string>,
		phmap::priv::Allocator<phmap::priv::Pair<const std::string, struct statcache_entry*>>,
		4,
		std::shared_mutex
		> files;
	phmap::parallel_node_hash_map<
		uintptr_t,
		std::string,
		phmap::priv::hash_default_hash<uintptr_t>,
		phmap::priv::hash_default_eq<uintptr_t>,
		phmap::priv::Allocator<phmap::priv::Pair<uintptr_t, const std::string>>,
		4,
		std::shared_mutex
		> files_reverse;

	// CTSL::HashMap<std::string, struct statcache_entry*> directories;
	// CTSL::HashMap<uintptr_t, std::string> directories_reverse;
	// CTSL::HashMap<std::string, struct statcache_entry*> files;
	// CTSL::HashMap<uintptr_t, std::string> files_reverse;
	
	//std::unordered_map<std::string, struct statcache_entry*> directories;
	//std::unordered_map<uintptr_t, std::string> directories_reverse;
	//std::unordered_map<std::string, struct statcache_entry*> files;
	//std::unordered_map<uintptr_t, std::string> files_reverse;
	struct statcache_entry root;
};

#endif /* STATCACHE_H */


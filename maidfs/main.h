#ifndef MAIN_H
#define MAIN_H

#define FUSE_USE_VERSION 35

#include <unordered_map>
#include <mutex>
#include <fstream>

#include <time.h>

#include "backing.h"
#include "CommandQueue.h"
#include "config.h"
#include "HashMap.h"
#include "statcache.h"
#include "tcpserver.h"

#include "parallel_hashmap/phmap.h"

#define MAIDFS_LOG_ERROR	0
#define MAIDFS_LOG_WARNING	1
#define MAIDFS_LOG_MSG		2
#define MAIDFS_LOG_INFO		3
#define MAIDFS_LOG_DEBUG	4

//<editor-fold desc="Fields">
extern config *cfg;

extern statcache stat_cache;

extern CommandQueue* command_queue;

extern std::ofstream logStream;
extern std::mutex log_m;

struct fdflag
{
	int flags;
	std::string rpath;
	backing *b;
	
	fdflag()
	{
		this->flags = 0;
	}
};


extern phmap::parallel_node_hash_map<
	int,
	struct fdflag,
	phmap::priv::hash_default_hash<int>,
	phmap::priv::hash_default_eq<int>,
	phmap::priv::Allocator<phmap::priv::Pair<const int, struct fdflag>>,
	4,
	std::shared_mutex
	> fdflags;

struct tmplink
{
	std::string oldrpath;
	bool isdir;
	unsigned long timeCreate;

	tmplink()
	{
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);

		this->timeCreate = ts.tv_sec;
	}
};
extern bool tmplinks_any;
extern CTSL::HashMap<std::string, struct tmplink> tmplinks;
//</editor-fold>

//<editor-fold desc="Helper Functions">
extern void maidfs_log(char level, std::string message, bool write_to_cout = false);
extern backing* getBacking(std::string rpath);
extern std::string getBackingPath(std::string path, bool nocache = false);
extern std::vector<std::string> getBackingPathAll(std::string path);
extern std::string getNewBackingPathSize(std::string path, unsigned long size);
extern std::string getNewBackingPath(std::string path);
extern int getBackingStat(std::string path, struct stat& stbuf, bool nocache = false);
extern int getStat(std::string path, struct stat &stbuf, bool nocache = false);
extern void cacheStat(std::string path, bool recursive = false);
extern int flushStat(std::string path, bool recursive = false);
extern int deleteStat(std::string path);
extern int reallocMoveFile(std::string path, std::string& rpath, ulong size);
extern bool isHidden(std::string path);
//</editor-fold>

#endif /* MAIN_H */

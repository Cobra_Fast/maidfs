#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <string>
#include <thread>

#include <arpa/inet.h>
#include <netinet/in.h>

#define MAXPACKET 4096

class tcpserver;

struct handle_arg
{
	tcpserver* inst;
	int cfd;
};

typedef void (*received)(std::string, struct handle_arg*);

class tcpserver
{
public:
	int sockfd, n, pid;
	struct sockaddr_in srvAddr;
	std::thread* t_receive;
	bool active;
	received callback;
	
	tcpserver(int port, received callback);
	void receive();
	virtual ~tcpserver();
private:
	void handle(void* arg);
};

#endif /* TCPSERVER_H */


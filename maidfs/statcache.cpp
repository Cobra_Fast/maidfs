
//#define FUSE_USE_VERSION 30

#include "main.h"

#include <algorithm>
#include <string>
#include <vector>

#include <fuse.h>
#include <sys/stat.h>

#include "helper.cpp"
#include "statcache.h"

statcache::statcache()
{
	
}

int statcache::add(std::string path, struct stat st)
{
	return this->add(path, st, getBacking(getBackingPath(path, true)));
}

/**
 * Add a stat entry to the stat cache.
 * @param path Full normalized and absolute path.
 * @param st fully initialized/filled stat struct
 * @return 0 on success, -1 on error (e.g. trying to add a child to an unexistant parent)
 */
int statcache::add(std::string path, struct stat st, backing* b)
{
	if (path == "/")
		return -1;
	
	if (this->exists(path))
		return -1;

	const unsigned short last_slash_pos = path.find_last_of('/');

	struct statcache_entry *n = new struct statcache_entry();
	n->st = st;
	n->name = path.substr(last_slash_pos + 1);
	n->b = b;

	std::string ppath(this->getParentPath(path, last_slash_pos));
	
	statcache_entry *e;
	if (last_slash_pos > 0 && !this->directories.find(ppath, e))
	{
		maidfs_log(MAIDFS_LOG_ERROR, "Cannot add '" + path + "' to statcache because its parent does not exist.");
		goto add_error;
	}
	
	if (S_ISDIR(st.st_mode))
	{
		if (last_slash_pos > 0)
		{
			e->child_dirs.push_back(n);
		}
		else
		{
			this->root.child_dirs.push_back(n);
		}

		this->directories.insert(std::pair(path, n));
		this->directories_reverse.replace((uintptr_t)n, path);
	}
	else
	{
		if (last_slash_pos > 0)
		{
			e->child_files.push_back(n);
		}
		else
		{
			this->root.child_files.push_back(n);
		}
		
		this->files.insert(std::pair(path, n));
		this->files_reverse.replace((uintptr_t)n, path);
	}
	
	return 0;
	
add_error:
		delete n;
		return -1;
}

/**
 * Delete an element from the cache.
 * @param path Full normalized and absolute path
 * @return 0 on success, otherwise error: -3 if the element has children, -2 if the parent element cannot be found, -1 if the element cannot be found.
 */
int statcache::del(std::string path)
{
	if (path == "")
		return -1;
	
	std::string ppath(this->getParentPath(path));
	
	statcache_entry *e;
	if (this->files.find(path, e))
	{
		if (!e->child_dirs.empty() || !e->child_files.empty())
			return -3;
		
		this->files.erase(path);
		this->files_reverse.erase((uintptr_t)e);
		
		if (!ppath.empty())
		{
			struct statcache_entry* pi;
			if (this->directories.find(ppath, pi))
			{
				pi->child_files.erase(std::remove(
					pi->child_files.begin(),
					pi->child_files.end(),
					e
				));
			}
			else
				return -2;
		}
		else
		{
			this->root.child_files.erase(std::remove(
				this->root.child_files.begin(),
				this->root.child_files.end(),
				e
			));
		}
		
		delete e;
		
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Removed file " + path + " from stat cache.");
#endif
		
		return 0;
	}
	
	if (this->directories.find(path, e))
	{
		if (!e->child_dirs.empty() || !e->child_files.empty())
			return -3;
		
		this->directories.erase(path);
		this->directories_reverse.erase((uintptr_t)e);
		
		if (!ppath.empty())
		{
			struct statcache_entry* pi;
			if (this->directories.find(ppath, pi))
			{
				pi->child_dirs.erase(std::remove(
					pi->child_dirs.begin(),
					pi->child_dirs.end(),
					e
				));
			}
			else
				return -2;
		}
		else
		{
			this->root.child_dirs.erase(std::remove(
				this->root.child_dirs.begin(),
				this->root.child_dirs.end(),
				e
			));
		}
		
		delete e;
		
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Removed directory " + path + " from stat cache.");
#endif
		
		return 0;
	}
	
	return -1;
}

int statcache::delr(std::string path)
{
	char type = 0;
	if (this->exists(path, type) == false)
		return -1;
	
	if (type == 1)
		return this->del(path);
	
	struct statcache_entry* i;
	if (!this->directories.find(path, i))
		return -1;
	
	while (i->child_files.size() > 0)
	{
		struct statcache_entry* e = i->child_files[0];
		std::string epath(addrtopath(e));
		if (epath == "")
		{
			maidfs_log(MAIDFS_LOG_WARNING, "Recursive stat deletion: child element seems to be already deleted.");
			i->child_files.erase(i->child_files.begin());
			continue;
		}
		int r = this->del(epath);
		if (r != 0)
		{
			maidfs_log(MAIDFS_LOG_WARNING, "Recursive stat deletion: del(" + epath + ") returned " + std::to_string(r));
			return r;
		}
	}
	
	while (i->child_dirs.size() > 0)
	{
		statcache_entry* e = i->child_dirs[0];
		std::string epath(addrtopath(e));
		if (epath == "")
		{
			maidfs_log(MAIDFS_LOG_WARNING, "Recursive stat deletion: child element seems to be already deleted.");
			i->child_dirs.erase(i->child_dirs.begin());
			continue;
		}
		int r = this->delr(epath);
		if (r != 0)
		{
			maidfs_log(MAIDFS_LOG_WARNING, "Recursive stat deletion: nested delr(" + epath + ") returned " + std::to_string(r));
			return r;
		}
	}
	
	this->del(path);
	
	return 0;
}

/**
 * Replace the stat struct in place without creating or deleting.
 * @param path Full normalized and absolute path
 * @param st New stat struct to be saved
 * @return 0 on success, -1 on error
 */
int statcache::repl(std::string path, struct stat st)
{
	struct statcache_entry* i;
	if (this->files.find(path, i))
	{
		i->st = st;
		return 0;
	}
	
	if (this->directories.find(path, i))
	{
		i->st = st;
		return 0;
	}
	
	return -1;
}

int statcache::repl(std::string path, backing* b)
{
	struct statcache_entry* i;
	if (this->files.find(path, i))
	{
		i->b = b;
		return 0;
	}

	if (this->directories.find(path, i))
	{
		i->b = b;
		return 0;
	}

	return -1;
}

/**
 * Get the stat struct for a path
 * @param path Full normalized and absolute path
 * @param st The stat struct output
 * @return 0 on success, -1 on error
 */
int statcache::get(std::string path, struct stat &st)
{
	struct statcache_entry* i;
	if (this->files.find(path, i))
	{
		st = i->st;
		return 0;
	}
	
	if (this->directories.find(path, i))
	{
		st = i->st;
		return 0;
	}
	
	return -1;
}

/**
 * Get the stat struct for a path if it describes a directory
 * @param path 
 * @param st The stat struct output
 * @return 0 on success, -1 on error 
 */
int statcache::getd(std::string path, struct stat &st)
{
	struct statcache_entry* i;
	if (this->directories.find(path, i))
	{
		st = i->st;
		return 0;
	}
	
	return -1;
}

/**
 * Get the stat struct for a path if it describes a file
 * @param path
 * @param st The stat struct output
 * @return 0 on success, -1 on error
 */
int statcache::getf(std::string path, struct stat &st)
{
	struct statcache_entry* i;
	if (this->files.find(path, i))
	{
		st = i->st;
		return 0;
	}
	
	return -1;
}

backing* statcache::getbacking(std::string path)
{
	struct statcache_entry* i;
	if (this->files.find(path, i))
		return i->b;
	
	if (this->directories.find(path, i))
		return i->b;
	
	return nullptr;
}

/**
 * Check if there is any item for a path
 * @param path Full normalized and absolute path
 * @return true if an element exists, false otherwise
 */
bool statcache::exists(std::string path)
{
	struct statcache_entry* e;
	return (this->files.find(path, e))
		|| (this->directories.find(path, e));
}

/**
 * Check if there is any item for a path and indicate its type
 * @param path Full normalized and absolute path
 * @param type Type output: 1=file, 2=directory, unchanged on error
 * @return 0 on success, -1 on error
 */
bool statcache::exists(std::string path, char &type)
{
	struct statcache_entry* e;
	if (this->files.find(path, e))
	{
		type = 1;
		return true;
	}
	
	if (this->directories.find(path, e))
	{
		type = 2;
		return true;
	}
	
	return false;
}

int statcache::filldir(std::string path, void* buf, fuse_fill_dir_t filler)
{
	struct statcache_entry* ie;
	
	if (path == "/")
	{
		ie = &this->root;
	}
	else
	{
		struct statcache_entry *i;
		if (!this->directories.find(path, i))
			return -1;
		ie = i;
	}
	
	for (struct statcache_entry* e: ie->child_dirs)
		filler(buf, e->name.c_str(), const_cast<struct stat*>(&e->st), 0, FUSE_FILL_DIR_PLUS);
	for (struct statcache_entry* e: ie->child_files)
		filler(buf, e->name.c_str(), const_cast<struct stat*>(&e->st), 0, FUSE_FILL_DIR_PLUS);

	return 0;
}

std::string statcache::getParentPath(std::string path, int last_slash_pos)
{
	if (last_slash_pos < 0)
		last_slash_pos = path.find_last_of('/');
	
	if (last_slash_pos <= 0)
		return "";
	
	std::vector<std::string> pseg = strsplit(path, '/');
	std::string ppath("");
	for (int i = 0; i < pseg.size() - 1; i++)
		ppath += "/" + pseg[i];
	return normalizePath(ppath);
}

std::string statcache::addrtopath(struct statcache_entry* e)
{
	std::string r("");
	
	std::string c1;
	if (this->files_reverse.find((uintptr_t)e, c1))
		r = c1;
	
	if (r.empty())
	{
		std::string c2;
		if (this->directories_reverse.find((uintptr_t)e, c2))
			r = c2;
	}
	
	return r;
}

statcache::~statcache()
{
	//for (std::pair<std::string, struct statcache_entry*> p: this->files)
	//	delete p.second;
	this->files.clear();
	//for (std::pair<std::string, struct statcache_entry*> p: this->directories)
	//	delete p.second;
	this->directories.clear();
}


#include <unistd.h>
#include <thread>
#include <sys/socket.h>
#include <sys/types.h>

#include "tcpserver.h"

tcpserver::tcpserver(int port, received callback)
{
	this->sockfd = socket(AF_INET, SOCK_STREAM, 0);
	this->srvAddr.sin_family = AF_INET;
	this->srvAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	this->srvAddr.sin_port = htons(port);
	bind(sockfd, (struct sockaddr *)&this->srvAddr, sizeof(this->srvAddr));
	listen(sockfd,5);
	this->active = true;
	this->callback = callback;
	this->t_receive = new std::thread([this] { this->receive(); });
	pthread_setname_np(this->t_receive->native_handle(), "tcpserver:recv");
}

void tcpserver::receive()
{
	struct sockaddr_in ca;
	socklen_t ss = sizeof(ca);
	while (this->active)
	{
		int newsockfd = accept(sockfd, (struct sockaddr*)&ca, &ss);
		struct handle_arg ha;
		ha.cfd = newsockfd;
		ha.inst = this;
		std::thread ht([this, &ha] { this->handle(&ha); });
		pthread_setname_np(ht.native_handle(), "tcpserver:hdl");
		ht.detach();
	}
}

void tcpserver::handle(void* arg)
{
	struct handle_arg *ha;
	ha = (struct handle_arg*)arg;
	char msg[MAXPACKET];
	
	while (true)
	{
		int n = recv(ha->cfd, msg, MAXPACKET, 0);
		if (n == 0 || n == -1)
		{
			close(ha->cfd);
			break;
		}
		
		msg[n] = 0;
		
		ha->inst->callback(std::string(msg), ha);
	}
}

tcpserver::~tcpserver()
{
	close(this->sockfd);
}


#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/backing.o \
	${OBJECTDIR}/config.o \
	${OBJECTDIR}/helper.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/statcache.o \
	${OBJECTDIR}/tcpserver.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-D_FILE_OFFSET_BITS=64
CXXFLAGS=-D_FILE_OFFSET_BITS=64

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=`pkg-config --libs fuse3`  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/maidfs

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/maidfs: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/maidfs ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/backing.o: backing.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 `pkg-config --cflags fuse3` -std=c++17  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/backing.o backing.cpp

${OBJECTDIR}/config.o: config.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 `pkg-config --cflags fuse3` -std=c++17  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/config.o config.cpp

${OBJECTDIR}/helper.o: helper.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 `pkg-config --cflags fuse3` -std=c++17  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/helper.o helper.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 `pkg-config --cflags fuse3` -std=c++17  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/statcache.o: statcache.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 `pkg-config --cflags fuse3` -std=c++17  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/statcache.o statcache.cpp

${OBJECTDIR}/tcpserver.o: tcpserver.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O3 `pkg-config --cflags fuse3` -std=c++17  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tcpserver.o tcpserver.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc

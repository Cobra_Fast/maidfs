#include <string>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <vector>
#include <stdlib.h>
#include <sys/types.h>

#include "config.h"
#include "backing.h"

config::config(std::string path)
{
	this->manage_port = 0;
	this->log_level = 0;
	this->multibacking = 0;
	this->preferexisting = 0;
	this->stat_cache = 0;
	this->stat_cache_prime = 0;
	this->rootmode = (mode_t)0755;
	this->rootuid = (uid_t)0;
	this->rootgid = (gid_t)0;
	
	this->parse(path);
}

void config::parse(std::string path)
{
	std::ifstream ifs(path);
	std::string line;
	char mode = 0;
	backing *bdev;
	while (std::getline(ifs, line))
	{
		if (line.empty() || line.front() == '#')
			continue;
		
		if (line.front() == '[' && line.back() == ']')
		{
			char c_section[32] = {0};
			char c_value[32] = {0};
			int sc = sscanf(line.c_str(), "[%[a-z]:%[^]]]", c_section, c_value);
			if (sc == 2)
			{
				std::string section(c_section);
				std::string value(c_value);
				
				if (mode == 1 && bdev != nullptr)
					this->bdirs.push_back(bdev);
				
				if (section == "backing")
				{
					bdev = new backing(value);
					mode = 1;
				}
				else
					mode = 254;
			}
			else
			{
				sc = sscanf(line.c_str(), "[%[a-z]]", c_section);
				if (sc != EOF)
				{
					std::string section(c_section);
					if (section == "general")
						mode = 0;
				}
			}
		}
		else
		{
			char c_key[32];
			char c_value[1024];
			int sc = sscanf(line.c_str(), "%[^=]=%s", c_key, c_value);
			if (sc == EOF)
				continue;
			std::string key(c_key);
			std::string value(c_value);
			switch (mode)
			{
				case 0: // general
					if (key == "log")
						this->log = value;
					else if (key == "log_level")
						this->log_level = (char)atoi(value.c_str());
					else if (key == "name")
						this->name = value;
					else if (key == "stat_cache")
						this->stat_cache = (char)atoi(value.c_str());
					else if (key == "stat_cache_prime")
						this->stat_cache_prime = (char)atoi(value.c_str());
					else if (key == "manage_port")
						this->manage_port = atoi(value.c_str());
					else if (key == "manage_pass")
						this->manage_pass = value;
					else if (key == "multibacking")
						this->multibacking = (char)atoi(value.c_str());
					else if (key == "rename_realloc")
						this->rename_realloc = (char)atoi(value.c_str());
					else if (key == "rootmode")
						this->rootmode = (mode_t)strtol(value.c_str(), NULL, 8);
					else if (key == "rootuid")
						this->rootuid = (uid_t)atoi(value.c_str());
					else if (key == "rootgid")
						this->rootgid = (gid_t)atoi(value.c_str());
					else if (key == "preferexisting")
						this->preferexisting = (char)atoi(value.c_str());
					else if (key == "hide")
						this->hidden.push_back(value);
					break;
				case 1: // backing
					if (key == "dev")
						bdev->dev = value;
					else if (key == "prefer_dir")
						bdev->preferred_dirs.push_back(value);
					else if (key == "minfree")
						bdev->minfree = strtoul(value.c_str(), NULL, 0);
					else if (key == "allow_empty")
						bdev->allow_empty = (char)atoi(value.c_str());
					else if (key == "fsc_enable")
						bdev->fsc_enable = (char)atoi(value.c_str());
					else if (key == "sealed")
						bdev->sealed = (char)atoi(value.c_str());
					break;
			}
		}
	}
	
	if (mode == 1 && bdev != nullptr)
		this->bdirs.push_back(bdev);
}

config::~config() { }


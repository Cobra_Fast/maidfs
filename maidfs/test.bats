#!/usr/bin/env bats
load /usr/lib/bats-assert/load.bash

@test "readdir /" {
	ls ./mountpoint/ > /dev/null
	assert_equal $? 0
}

@test "touch /testfile" {
	touch ./mountpoint/testfile
	assert_equal $? 0
	assert [ -e ./mountpoint/testfile ]
}

@test "readdir / again" {
	run ls ./mountpoint/
	assert_equal $? 0
	assert_output --partial testfile
}

@test "write /testfile" {
	DATA="testdata"
	echo $DATA > ./mountpoint/testfile
	assert_equal $(cat ./mountpoint/testfile) $DATA
	assert_equal $? 0
}

@test "move /testfile to /filetest" {
	mv ./mountpoint/testfile ./mountpoint/filetest
	assert_equal $? 0
}

@test "delete /filetest" {
	rm ./mountpoint/filetest
	assert_equal $? 0
	refute [ -e ./mountpoint/filetest ]
}

@test "create directory /testdir" {
	mkdir ./mountpoint/testdir
	assert_equal $? 0
	assert [ -d ./mountpoint/testdir ]
}

@test "readdir /testdir" {
	ls ./mountpoint/testdir > /dev/null
	assert_equal $? 0
}

@test "crate file /testdir/testfile by writing" {
	DATA="testdata"
	echo $DATA > ./mountpoint/testdir/testfile
	assert [ -f ./mountpoint/testdir/testfile ]
	assert_equal $(cat ./mountpoint/testdir/testfile) $DATA
}

@test "readdir /testdir again" {
	run ls ./mountpoint/testdir/
	assert_output --partial testfile
}

@test "delete /testdir/testfile" {
	rm ./mountpoint/testdir/testfile
	assert_equal $? 0
	refute [ -f ./mountpoint/testdir/testfile ]
}

@test "recreate /testdir/testfile" {
	DATA="testdata"
	echo $DATA > ./mountpoint/testdir/testfile
	assert [ -f ./mountpoint/testdir/testfile ]
	assert_equal $(cat ./mountpoint/testdir/testfile) $DATA
}

@test "rename /testdir to /dirtest" {
	mv ./mountpoint/testdir ./mountpoint/dirtest
	assert_equal $? 0
	assert [ -d ./mountpoint/dirtest ]
}

@test "delete /dirtest recursively" {
	rm -R ./mountpoint/dirtest
	assert_equal $? 0
	refute [ -d ./mountpoint/dirtest ]
}

@test "create /d0/testfile" {
	mkdir -p ./mountpoint/d0
	assert [ -d ./mountpoint/d0 ]
	dd if=/dev/urandom of=./mountpoint/d0/testfile bs=64k count=4
	FILESIZE=$(stat -c%s ./mountpoint/d0/testfile)
	assert [ -f ./mountpoint/d0/testfile ]
	assert [ $FILESIZE -gt 0 ]
}

@test "move /d0/testfile across devices to /d1/testfile" {
	mkdir -p ./mountpoint/d1
	assert [ -d ./mountpoint/d1 ]
	mv ./mountpoint/d0/testfile ./mountpoint/d1/testfile
	refute [ -f ./mountpoint/d0/testfile ]
	assert [ -f ./mountpoint/d1/testfile ]
	FILESIZE=$(stat -c%s ./mountpoint/d1/testfile)
	assert [ $FILESIZE -gt 0 ]
}

@test "delete /d1/testfile" {
	rm ./mountpoint/d1/testfile
	refute [ -f ./mountpoint/d0/testfile ]
	refute [ -f ./mountpoint/d1/testfile ]
}

@test "create /torture directory" {
	mkdir ./mountpoint/torture
	assert_equal $? 0
	assert [ -d ./mountpoint/torture ]
}

@test "torture: create, write, and verify 1000 files" {
	for i in {0..1000}; do
		echo $i > ./mountpoint/torture/torture$i
		assert_equal $(cat ./mountpoint/torture/torture$i) $i
	done
}

@test "torture: delete previously created files" {
	rm ./mountpoint/torture/torture*
	assert_equal $? 0
	refute [ -f ./mountpoint/torture/torture500 ]
}

@test "torture: create, write, verify, and delete 1000 files in parallel" {
	for t in {0..4}; do
		(
			for i in {0..200}; do
				echo $t$i > ./mountpoint/torture/torture$t.$i;
				assert_equal $(cat ./mountpoint/torture/torture$t.$i) $t$i;
				rm ./mountpoint/torture/torture$t.$i
				refute [ -f ./mountpoint/torture/torture$t.$i ]
			done
		) &
	done
	for job in `jobs -p`; do
		wait $job
	done
}

@test "delete /torture directory" {
	rmdir ./mountpoint/torture
	assert_equal $? 0
	refute [ -d ./mountpoint/torture ]
}

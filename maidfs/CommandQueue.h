#ifndef COMMANDQUEUE_H
#define COMMANDQUEUE_H

#include <chrono>
#include <functional>
#include <mutex>
#include <thread>
#include <queue>

class CommandQueue
{
private:
	std::queue<std::function<void()>> q;
	std::mutex q_m;
	std::thread w;
	const int min_wait_time = 25;
	const int max_wait_time = 500;
	int consecutive_waits = 0;
	
	void worker()
	{
		this->w = std::thread([this]() {
			while (true)
			{
				if (!this->q.empty())
				{
					this->q_m.lock();
					std::function<void()> func = this->q.front();
					this->q.pop();
					this->q_m.unlock();
					func();
					this->consecutive_waits = 0;
				}
				else
				{
					int wait_time = this->consecutive_waits < 2 ? this->min_wait_time : this->consecutive_waits * this->min_wait_time;
					if (wait_time > this->max_wait_time)
						wait_time = this->max_wait_time;
					std::this_thread::sleep_for(std::chrono::milliseconds(wait_time));
					this->consecutive_waits++;
				}
			}
		});
		pthread_setname_np(this->w.native_handle(), "cq::worker");
		pthread_setschedprio(this->w.native_handle(), 5);
		this->w.detach();
	}
public:
	CommandQueue()
	{
		this->worker();
	}
	
	template<typename _Callable, typename... _Args>
	void add(_Callable&& __f, _Args&&... __args)
	{
		std::function<void()> func = std::bind(
			std::forward<_Callable>(__f),
			std::forward<_Args>(__args)...
		);
		
		this->q_m.lock();
		this->q.push(func);
		this->q_m.unlock();
	};
	
	void wait_finish()
	{
		while (this->size() > 0)
			std::this_thread::sleep_for(std::chrono::milliseconds(this->min_wait_time));
	};
	
	int size()
	{
		return this->q.size();
	};
};

#endif /* COMMANDQUEUE_H */


#include <cstring>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <iterator>

#include <dirent.h>
#include <ftw.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/wait.h>

#include "main.h"

static int run(const char* execpath, char* const* args)
{
#ifdef DEBUG
			std::string debug_args;
			for (int i = 0; args[i] != NULL; i++)
			{
				debug_args += args[i];
				debug_args += " ";
			}
			maidfs_log(MAIDFS_LOG_DEBUG, "Executing: (" + std::string(execpath) + ") " + debug_args);
#endif

	pid_t pid = fork();

	switch (pid)
	{
		case -1:
			maidfs_log(MAIDFS_LOG_ERROR, "Forking into command '" + std::string(execpath) + "' failed.");
			return -1;
		case 0:
			exit(execv(execpath, args));
			break;
		default:
			int stat_val;
			waitpid(pid, &stat_val, 0);
			return WEXITSTATUS(stat_val);
	}

	return -2;
}

static std::string findbinary(std::string binaryname)
{
	std::vector<std::string> dirs = {
		"/bin/",
		"/usr/bin/",
		"/sbin/",
		"/usr/sbin/",
		"/usr/local/bin/",
		"/usr/local/sbin/",
	};

	// TODO cache result, rescan if cached path is wrong
	struct stat st;
	for (std::string dir: dirs)
	{
		std::string candidate = std::string(dir + binaryname);
		if (stat(candidate.c_str(), &st) == 0 && st.st_mode & S_IXUSR)
		{
#ifdef DEBUG
			maidfs_log(MAIDFS_LOG_DEBUG, "Found binary for " + binaryname + " at " + candidate);
#endif
			return candidate;
		}
	}

	return "/bin/false";
}

/**
 * Split a string by a character
 * @param str Input string
 * @param sep Split character (separator)
 * @return vector of segments without split character
 */
static std::vector<std::string> strsplit(const std::string str, const char sep)
{
	std::vector<std::string> segments;
	std::size_t start = 0, end = 0;
	
	while ((end = str.find(sep, start)) != std::string::npos)
	{
		segments.push_back(str.substr(start, end - start));
		start = end + 1;
	}
	
	segments.push_back(str.substr(start));
	return segments;
}

/**
 * Split a string by a character where not preceded by an escape character
 * @param str Input string
 * @param sep Split character (separator)
 * @param esc Escape character
 * @return vector of segments without split or escape (near skipped splits) character
 */
static std::vector<std::string> strsplit(std::string str, const char sep, const char esc)
{
	std::vector<std::string> segments;
	std::size_t seg_start = 0, search_start = 0, end = 0;
	
	while ((end = str.find(sep, search_start)) != std::string::npos)
	{
		if (end > 0 && str[end - 1] == esc)
			str = str.erase(end - 1, 1);
		else
		{
			segments.push_back(str.substr(seg_start, end - seg_start));
			seg_start = end + 1;
		}
		search_start = end + 1;
	}
	
	segments.push_back(str.substr(seg_start));
	return segments;
}

/**
 * Converts any POSIX path (e.g. /a/d/../b/.//c/) to absolute /a/b/c format.
 * @param path Any POSIX valid path beginning with /
 * @return Path in absolute /a/b/c format.
 */
static std::string normalizePath(std::string path)
{
	if (path == "/")
		return "/";
	
	if (path[0] != '/') // full relative paths not supported due to lack of context
		return "";
	
	std::vector<std::string> segments = strsplit(path, '/');
	while (segments[0] == "." || segments[0] == "..")
		segments.erase(segments.begin());
	
	for (int i = 0; i < segments.size(); i++)
	{
		if (segments[i] == "." || segments[i].empty())
			segments.erase(segments.begin() + (i--));
		else if (segments[i] == "..")
		{
			--i;
			segments.erase(std::next(segments.begin(), i), std::next(segments.begin(), i+2));
		}
	}
	
	std::string r;
	for (int i = 0; i < segments.size(); i++)
		r += "/" + segments[i];
	
	return r;
}

/**
 * Recursive mkdir. EEXIST errno is ignored.
 * @param c_dir Path of directories to create.
 * @param mode mode
 * @return 0 on success, -errno on failure (EEXIST ignored).
 */
static int mkdirr(const char *c_dir, mode_t mode, uid_t uid, gid_t gid)
{
	std::string dir(c_dir);
	if (dir.back() == '/')
		dir.pop_back();
	
	std::string tree;
	for (std::string dir: strsplit(dir, '/'))
	{
		tree += "/" + dir;
		if (mkdir(tree.c_str(), mode) != 0)
		{
			if (errno == EEXIST)
				continue; // Do not chown() preexisting directories.
			else
				return -errno;
		}
		chown(tree.c_str(), uid, gid);
	}
	
	return 0;
}

/**
 * Get the current umask.
 * @return umask
 */
static mode_t _getumask()
{
	mode_t mask = umask(0);
	umask(mask);
	return mask;
}

/**
 * Retrieve the real path of a file descriptor
 * @param fd file descriptor
 * @return real system path
 */
static std::string fdtopath(const int fd)
{
	std::string fdp("/proc/self/fd/" + std::to_string(fd));
	char* fn = new char[255]();
	
	int r = readlink(fdp.c_str(), fn, 255);
	
	if (r == -1)
		return "";
	
	std::string rs(fn);
	delete fn;
	
	return rs;
}

static int move(std::string oldpath, std::string newpath)
{
	char* oldpath_c = const_cast<char*>(oldpath.c_str());
	char* newpath_c = const_cast<char*>(newpath.c_str());
	char* args[] = {
		const_cast<char*>((const char*)"mv"),
		oldpath_c,
		newpath_c,
		NULL
	};
	return run(findbinary("mv").c_str(), args);
}

/**
 * Copies a file.
 * @param oldpath
 * @param newpath
 * @return 0 on success, -errno on error.
 */
static int copy(std::string oldpath, std::string newpath)
{
	char* oldpath_c = const_cast<char*>(oldpath.c_str());
	char* newpath_c = const_cast<char*>(newpath.c_str());
	char* args[] = { 
		const_cast<char*>((const char*)"cp"),
		const_cast<char*>((const char*)"-a"),
		oldpath_c,
		newpath_c,
		NULL
	};
	return run(findbinary("cp").c_str(), args);
}

/**
 * Copies the first n bytes of a file.
 * @param oldpath
 * @param newpath
 * @param size
 * @return 0 on success, -errno on error.
 */
static int copy(std::string oldpath, std::string newpath, size_t size)
{
	std::ifstream inf(oldpath);
	std::ofstream ouf(newpath);
	
	if (ouf.fail())
		return -errno;
	
	if (inf.fail())
		return -errno;
	
	const ulong bufsize = 4096;
	char *buf = new char[bufsize];
	while (size > 0)
	{
		const ulong chunksize = size > bufsize ? bufsize : size;
		inf.read(buf, chunksize);
		ouf.write(buf, chunksize);
		size -= chunksize;
	}
	
	inf.close();
	ouf.close();
	
	delete[] buf;
	
	if (ouf.fail() || inf.fail())
	{
		unlink(newpath.c_str());
		return -EIO;
	}
	
	return 0;
}

/**
 * Copy directory recursively.
 * @return 0 on success, -errno on error
 */
static int copyd(std::string oldpath, std::string newpath)
{
	char* oldpath_c = const_cast<char*>(oldpath.c_str());
	char* newpath_c = const_cast<char*>(newpath.c_str());
	char* args[] = {
		const_cast<char*>((const char*)"cp"),
		const_cast<char*>((const char*)"-aR"),
		oldpath_c,
		newpath_c,
		NULL
	};
	return run(findbinary("cp").c_str(), args);
}

/**
 * Autodetect whether to copy a file or a directory recursively.
 * @return 0 on success, other on error
 */
static int copya(std::string oldpath, std::string newpath)
{
	return -1; // TODO
}

/**
 * Check if a directory is empty.
 * @param rpath
 * @return true if directory is empty or error, false if not empty.
 */
static bool dir_is_empty(std::string rpath)
{
	int i = 0;
	struct dirent *d;
	
	DIR *dir = opendir(rpath.c_str());
	if (dir == NULL)
		return true;
	
	while ((d = readdir(dir)) != NULL)
		if (++i > 2)
			break;
	
	closedir(dir);
	
	return (i <= 2);
}

static bool file_exists(std::string path)
{
	return (access(path.c_str(), F_OK) != -1);
}

static bool str_begins_with(std::string haystack, std::string prefix)
{
	return (haystack.substr(0, prefix.size()) == prefix);
}

/**
 * Recursively traverse a directory.
 * @param directory Directory to traverse.
 * @param callback Called for each item params are string full path and uchar d_type.
 */
static void dirtrav(std::string directory, std::function<void(std::string, unsigned char)> callback)
{
	directory = normalizePath(directory);
	DIR* dp = opendir(directory.c_str());
	
	if (dp == NULL)
		return;
	
	callback(directory, DT_DIR);
	
	dirent* de;
	
	while ((de = readdir(dp)) != NULL)
	{
		std::string d_name(de->d_name);
		if (d_name == "." || d_name == "..")
			continue;
		
		if (de->d_type == DT_DIR)
			dirtrav(directory + "/" + d_name, callback);
		else
			callback(directory + "/" + d_name, de->d_type);
	}
	
	closedir(dp);
}

static bool timespeclt(const timespec left, const timespec right)
{
	if (left.tv_sec == right.tv_sec)
		return left.tv_nsec < right.tv_nsec;
	else
		return left.tv_sec < right.tv_sec;
}

static struct timeval timespectotimeval(struct timespec ts)
{
	struct timeval tv;
	
	if (ts.tv_nsec == UTIME_NOW)
	{
		gettimeofday(&tv, NULL);
		return tv;
	}
	
	if (ts.tv_nsec == UTIME_OMIT) // this needs to be handled outside of this function
	{
		tv.tv_usec = tv.tv_sec = 0;
		return tv;
	}
	
	tv.tv_sec = ts.tv_sec;
	tv.tv_usec = ts.tv_nsec / 1000;
	
	while (tv.tv_usec >= 1000000)
	{
		tv.tv_sec += 1;
		tv.tv_usec -= 1000000;
	}
	
	return tv;
}

static std::vector<std::string> allPaths(std::string path, bool exclude_self = false)
{
	std::vector<std::string> r;
	std::vector<std::string> seg(strsplit(path, '/'));
	std::string cur("");
	
	for (int x = seg.size() - 1; x >= 0; x--)
	for (int y = 0; y <= x; y++)
	{
		if (seg[y].empty())
			continue;
		
		cur += "/" + seg[y];
		
		if ((exclude_self == true && y == (x - 1)) || (exclude_self == false && y == x))
		{
			r.push_back(cur);
			cur = "";
		}
	}
	
	return r;
}
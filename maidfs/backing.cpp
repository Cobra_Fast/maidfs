#include <mutex>

#include <sys/statvfs.h>

#include "backing.h"

backing::backing(std::string path)
{
	this->path = path;
}

backing::backing(const backing& orig)
{
	this->path = orig.path;
	this->dev = orig.dev;
	this->minfree = orig.minfree;
	this->preferred_dirs = orig.preferred_dirs;
	this->allow_empty = orig.allow_empty;
	this->sealed = orig.sealed;
	this->fsc_enable = orig.fsc_enable;
}

unsigned long backing::getFreeSpace()
{
	bool result_get = false;
	unsigned long result;
	struct timespec ts;

	if (this->sealed > 0)
		return -1;

	if (this->fsc_enable > 0)
	{
		std::lock_guard<std::mutex> lg(this->fsc_m);
		if (this->fsc_enable > 1)
		{
			clock_gettime(CLOCK_MONOTONIC, &ts);

			if (ts.tv_sec < (this->fsc_t + this->fsc_enable))
			{
				// cache is still valid
				result = this->fsc_v;
				result_get = true;
			}
		}
		else if (this->fsc_t > 0)
		{
			result = this->fsc_v;
			result_get = true;
		}
	}

	if (!result_get)
	{
		struct statvfs sf;
		int r = statvfs(this->path.c_str(), &sf);
		if (r == -1)
			return (unsigned long)0;
		result = (sf.f_frsize * sf.f_bavail);

		if (this->fsc_enable > 0)
		{
			std::lock_guard<std::mutex> lg(this->fsc_m);
			this->fsc_v = result;
			if (this->fsc_enable > 1)
			{
				if (clock_gettime(CLOCK_MONOTONIC, &ts) == 0)
					this->fsc_t = ts.tv_sec;
				else
					this->fsc_t = 1;
			}
			else
				this->fsc_t = 1;
		}
	}

	return result;
}

void backing::invalidateFreeSpaceCache(int changeSizeHint, bool force)
{
	if (this->fsc_enable == 0)
		return;

	if (force)
	{
		std::lock_guard<std::mutex> lg(this->fsc_m);
		this->fsc_t = 0;
		return;
	}

	this->fsc_v += changeSizeHint;
}

backing::~backing()
{
	this->preferred_dirs.clear();
}


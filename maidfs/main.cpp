/**
 * maidfs <https://gitlab.com/Cobra_Fast/maidfs>
 * Started by Cobra_Fast <https://ezl.re> in 08/2017.
 */

#define MAIDFS_VERSION "0.2"
#include "main.h"

#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <mutex>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <shared_mutex>

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <linux/fs.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/time.h>
#include <sys/types.h>

#include "CommandQueue.h"
#include "config.h"
#include "backing.h"
#include "HashMap.h"
#include "statcache.h"
#include "helper.cpp"
#include "tcpserver.h"
#include "version.h"

config *cfg;

statcache stat_cache;

CommandQueue* command_queue;

std::ofstream logStream;
std::mutex log_m;

phmap::parallel_node_hash_map<
	int,
	struct fdflag,
	phmap::priv::hash_default_hash<int>,
	phmap::priv::hash_default_eq<int>,
	phmap::priv::Allocator<phmap::priv::Pair<const int, struct fdflag>>,
	4,
	std::shared_mutex
	> fdflags;

bool tmplinks_any;
CTSL::HashMap<std::string, struct tmplink> tmplinks;

static tcpserver *mansrv;

// <editor-fold desc="Helper functions">

/**
 * Logs a message if the configured log level is equal or higher than the message level.
 * @param level Level of the message.
 * @param message Message.
 * @param write_to_cout Write message to stdout regardless of level.
 */
void maidfs_log(char level, std::string message, bool write_to_cout)
{
	if (write_to_cout == true)
		std::cout << message << std::endl;
		
	if (logStream.is_open())
	{
		if (level > cfg->log_level)
			return;
		
		log_m.lock();
		
		auto t = std::time(nullptr);
		auto tm = *std::localtime(&t);
		
		logStream << std::put_time(&tm, "%F %T");
		logStream << " maidfs [";
		logStream << cfg->name;
		logStream << "] ";
		
		if (level == 2)
			logStream << ": ";
		else if (level == 3)
			logStream << "(info): ";
		else if (level == 4)
			logStream << "(debug): ";
		else if (level == 1)
			logStream << "(warn): ";
		else if (level == 0)
			logStream << "(error): ";
		else
			logStream << "(unknown): ";
		
		logStream << message;
		logStream << std::endl;
		
		log_m.unlock();
	}
}

/**
 * Get the backing object from a real path.
 * @param rpath Real system-absolute path.
 * @return backing object pointer
 */
backing* getBacking(std::string rpath)
{
	for (backing *b: cfg->bdirs)
	{
		if (rpath.substr(0, b->path.size()) == b->path)
			return b;
	}
	
	return nullptr;
}

/**
 * Returns the real path for a given virtual path.
 * @param path Virtual path within maidfs. (fuse-absolute)
 * @param nocache Ignore cache and force retrieval from backing
 * @return Real path outside of maidfs.
 */
std::string getBackingPath(std::string path, bool nocache)
{
	backing *bdev;
	bool found = false;
	
	if (nocache == false && cfg->stat_cache == 1 && cfg->stat_cache_prime == 1)
	{
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Using stat cache to resolve backing path of " + path);
#endif
		backing* b = stat_cache.getbacking(path);
		if (b != nullptr)
		{
			maidfs_log(MAIDFS_LOG_INFO, "Found backing path for " + path + " on " + b->path);
			return std::string(b->path + path);
		}
		else
		{
#ifdef DEBUG
			maidfs_log(MAIDFS_LOG_DEBUG, "Could not find backing path (not in primed stat cache) for " + path);
#endif
			return "";
		}
	}
	
	for (int i = 0; i < cfg->bdirs.size(); i++)
	{
		backing *b = cfg->bdirs[i];
		std::string realpath(b->path + path);
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Checking " + realpath + " as potential backing path for " + path);
#endif
		struct stat st;
		int statr = lstat(realpath.c_str(), &st);
		if (statr == 0)
		{
			bdev = b;
			found = true;
			break;
		}
	}
	
	if (!found)
	{
		maidfs_log(MAIDFS_LOG_INFO, "Could not find backing path for " + path);
		return "";
	}
	
	maidfs_log(MAIDFS_LOG_INFO, "Found backing path for " + path + " on " + bdev->path);
	
	return normalizePath(bdev->path + path);
}

/**
 * Returns all real paths for a given virtual path.
 * @param path Virtual path within maidfs. (fuse-absolute)
 * @return All real paths outside of maidfs.
 */
std::vector<std::string> getBackingPathAll(std::string path)
{
	std::vector<std::string> r;
	std::vector<struct stat> s;
	
	for (int i = 0; i < cfg->bdirs.size(); i++)
	{
		backing *b = cfg->bdirs[i];
		std::string rpath(normalizePath(b->path + path));
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Checking " + rpath + " as potential backing path for " + path);
#endif
		struct stat st;
		int statr = lstat(rpath.c_str(), &st);
		if (statr == 0)
		{
			r.push_back(rpath);
			s.push_back(st);
		}
	}
	
	for (int i = 1; i < s.size(); i++)
	{
		if (s[0].st_size != s[i].st_size || s[0].st_uid != s[i].st_uid || s[0].st_gid != s[i].st_gid || s[0].st_mode != s[i].st_mode)
		{
			// r.clear();
			maidfs_log(MAIDFS_LOG_WARNING, "File " + path + " is multi-backed with differing files!");
		}
	}
	
	return r;
}

/**
 * Returns the best real path for a given non-existant virtual path.
 * @param path Virtual non-existant/new path within maidfs. (fuse-absolute)
 * @param size Bytesize of new object.
 * @return Best real path outside of maidfs
 */
std::string getNewBackingPathSize(std::string path, unsigned long size)
{
	int lastslash = path.find_last_of('/');
	std::string dpath;
	if (lastslash == 0)
		dpath = "/";
	else
		dpath = path.substr(0, lastslash);
	std::string rpath;
	
	// check prefer_dir first
	for (backing *b: cfg->bdirs)
	{
		b->invalidateFreeSpaceCache();

		for (std::string pdir: b->preferred_dirs)
		{
			if (dpath.substr(0, pdir.size()) == pdir
				&& (b->getFreeSpace() - (signed long)size) > b->minfree)
			{
				rpath = b->path + path;
				return normalizePath(rpath);
			}
		}
	}
	
	// check preferexisting
	if (cfg->preferexisting == 1)
	{
		for (backing *b: cfg->bdirs)
		{
			if ((b->getFreeSpace() - (signed long)size) > b->minfree)
			{
				for (std::string ppath: allPaths(path, true))
				{
					std::string cndt(b->path + ppath);
					struct stat st;
					if (lstat(cndt.c_str(), &st) != 0)
						continue;
					
					rpath = b->path + path;
					return normalizePath(rpath);
				}
			}
		}
	}
	
	// fallback: pick first drive with enough space
	for (backing *b: cfg->bdirs)
	{
		if ((b->getFreeSpace() - (signed long)size) > b->minfree)
		{
			rpath = b->path + path;
			return normalizePath(rpath);
		}
	}
	
	maidfs_log(MAIDFS_LOG_WARNING, "Could not find new backing path for " + path + ".");
	
	return "";
}

/**
 * Returns the best real path for a given non-existant virtual path.
 * @param path Virtual non-existant/new path within maidfs. (fuse-absolute)
 * @return Best real path outside of maidfs
 */
std::string getNewBackingPath(std::string path)
{
	return getNewBackingPathSize(path, 0);
}

/**
 * Get a stat struct of a real file.
 * @param path Virtual path within maidfs. (fuse-absolute)
 * @param stbuf Ref/out stat struct.
 * @return errno
 */
int getBackingStat(std::string path, struct stat& stbuf, bool nocache)
{
	if (isHidden(path))
	{
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Not getting stat for " + path + " as it's hidden by config.");
#endif
		return -ENOENT;
	}
	
	std::string bpath(getBackingPath(path, nocache));
	if (bpath == "")
		return -ENOENT;
	struct stat st;
	int r = lstat(bpath.c_str(), &st);
	if (r == 0)
		stbuf = st;
	return r;
}

/**
 * Get a stat struct of a virtual file.
 * @param path Virtual path within maidfs. (fuse-absolute)
 * @param stbuf Ref/out stat struct.
 * @param nocache 
 * @return errno
 */
int getStat(std::string path, struct stat &stbuf, bool nocache)
{
	if (nocache == false && cfg->stat_cache == 1 && stat_cache.exists(path))
	{
		stat_cache.get(path, stbuf);
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Stat for " + path + " retrieved from cache.");
#endif
		return 0;
	}
	
	struct stat st;
	int r = getBackingStat(path, st, nocache);
	if (r == 0)
	{
		stbuf = st;
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Stat for " + path + " retrieved from backing.");
#endif
		
		if (cfg->stat_cache == 1)
		{
			stat_cache.add(path, st);
#ifdef DEBUG
			maidfs_log(MAIDFS_LOG_DEBUG, "Stat for " + path + " inserted into cache.");
#endif
		}
	}
	return r;
}

void cacheStatd(std::string path)
{
	std::string rpath(getBackingPath(path));
	DIR* d = opendir(rpath.c_str());
	struct dirent* de;
	
	while ((de = readdir(d)) != NULL)
	{
		if (std::string(de->d_name) == "." || std::string(de->d_name) == "..")
			continue;
		
		cacheStat(normalizePath(path + "/" + de->d_name), true);
	}
	
	closedir(d);
}

void cacheStat(std::string path, bool recursive)
{
	if (cfg->stat_cache == 1)
	{
		struct stat st;
		int r = getBackingStat(path, st, true);
		if (r == 0)
		{
			stat_cache.add(path, st);
#ifdef DEBUG
			maidfs_log(MAIDFS_LOG_DEBUG, "Stat for " + path + " inserted into cache.");
#endif
			if (recursive == true && S_ISDIR(st.st_mode))
				cacheStatd(path);
		}
	}
}

/**
 * Update a stat cache entry while also deleting from cache accordingly
 * @param path Virtual path within maidfs. (fuse-absolute)
 * @return 0 if removed, -1 if not found or not removed.
 */
int flushStat(std::string path, bool recursive)
{
	if (cfg->stat_cache == 1 && stat_cache.exists(path))
	{
		if (recursive == false)
		{
			struct stat st;
			if (getBackingStat(path, st, true) != 0)
				return -1;
			return stat_cache.repl(path, st);
		}
		
		stat_cache.delr(path);
		
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Stat for " + path + " deleted from cache, but eligible for immediate recaching.");
#endif
		
		if (cfg->stat_cache_prime == 1)
			cacheStat(path, recursive);
		
		return 0;
	}
	
	return -1;
}

/**
 * Remove a path from the stat cache. (without re-caching for stat_cache_prime=1)
 * @param path Virtual path within maidfs. (fuse-absolute)
 * @return 0 if removed, -1 if not found or not removed.
 */
int deleteStat(std::string path)
{
	if (cfg->stat_cache == 1 && stat_cache.exists(path))
	{
		stat_cache.del(path);
		
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Stat for " + path + " deleted from cache.");
#endif
		
		return 0;
	}
	
	return -1;
}

static void primeStatCache_td(std::string basepath, std::string d, std::vector<std::pair<std::string, struct stat>> &chunk)
{
	DIR *dir = opendir(d.c_str());
	
	if (!dir)
		return;
	
	struct dirent *de;
	while (de = readdir(dir))
	{
		std::string name(de->d_name);
		
		if (name == "." || name == "..")
			continue;
		
		std::string rpath(d + "/" + de->d_name);
		
		struct stat st;
		int r = lstat(rpath.c_str(), &st);
		if (r == -1)
		{
			cfg->stat_cache_prime = 0;
			maidfs_log(MAIDFS_LOG_WARNING, "Caught lstat error " + std::to_string(errno) + " during stat cache priming, element and child elements not cached! Priming disabled for safety.");
			continue;
		}
		
		std::string path = normalizePath(rpath.substr(basepath.size(), std::string::npos));
		if (isHidden(path))
		{
#ifdef DEBUG
			maidfs_log(MAIDFS_LOG_DEBUG, "Not caching " + path + " because it's hidden by config.");
#endif
			continue;
		}
		
		chunk.push_back(std::pair<std::string, struct stat>(path, st));
		
		if (S_ISDIR(st.st_mode))
			primeStatCache_td(basepath, rpath, chunk);
	}
	
	if (closedir(dir) == -1)
	{
		cfg->stat_cache_prime = 0;
		maidfs_log(MAIDFS_LOG_WARNING, "Caught closedir error " + std::to_string(errno) + " during stat cache priming. Priming disabled for safety.");
	}
}

static void primeStatCache_t(backing *b)
{
	std::vector<std::pair<std::string, struct stat>> chunk;
	
	primeStatCache_td(b->path, b->path, chunk);
	
	maidfs_log(MAIDFS_LOG_INFO, "Stat priming found " + std::to_string(chunk.size()) + " items in " + b->path);
	
	for (std::pair<std::string, struct stat> p: chunk)
		stat_cache.add(p.first, p.second, b);
}

static int primeStatCache()
{
	maidfs_log(MAIDFS_LOG_MSG, "Stat cache priming started. This can take a while...", true);
	
	std::thread ts[cfg->bdirs.size()];
	
	for (int i = 0; i < cfg->bdirs.size(); i++)
	{
		backing *b = cfg->bdirs[i];
		ts[i] = std::thread(primeStatCache_t, b);
		pthread_setname_np(ts[i].native_handle(), "primestatcache");
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Stat priming thread started for " + b->path);
#endif
	}
	
	for (int i = 0; i < cfg->bdirs.size(); i++)
		ts[i].join();
	
	maidfs_log(MAIDFS_LOG_MSG, "Stat cache priming finished.");
	
	return 0;
}

int reallocMoveFile(std::string path, std::string& rpath, ulong size)
{
	backing *b = getBacking(rpath);
	
	if (b->getFreeSpace() - size <= b->minfree)
	{
		std::string rpath_new(getNewBackingPathSize(path, size));
		maidfs_log(MAIDFS_LOG_MSG, "Moving file " + path + " to new location " + rpath_new + ".");
		int rn = move(rpath, rpath_new);
		if (rn != 0)
			return -errno;
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "reallocMoveFile(): Successfully moved " + path + " from " + rpath + " to " + rpath_new + ".");
#endif
		rpath = rpath_new;
	}
	else
	{
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "reallocMoveFile(): Did not move " + path + ".");
#endif
		return 1;
	}
	
	return 0;
}

bool isHidden(std::string path)
{
	for (std::string prefix: cfg->hidden)
		if (path.rfind(prefix, 0) == 0) // BUG sounds like lfind might be better?
			return true;
	return false;
}

void moveBackingBackground(std::string old_relpath, backing* old_bdir, std::string new_relpath, backing* new_bdir)
{
	command_queue->add([&](std::string old_relpath, backing* old_bdir, std::string new_relpath, backing* new_bdir){
		std::string old_rpath(old_bdir->path + old_relpath);
		std::string new_rpath(new_bdir->path + new_relpath);
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Starting background move from '" + old_bdir->path + "/" + old_relpath + "' to '" + new_bdir->path + "/" + new_relpath + "'.");
#endif
		struct stat st;
		if (lstat(old_rpath.c_str(), &st) != 0)
			return;
		if (!S_ISDIR(st.st_mode))
			return;

		// fail early if any containing file is opened for writing

		std::vector<std::string> items;
		dirtrav(old_rpath, [&](std::string full_name, unsigned char d_type){
			// create entries in HashMap tmplink.
			// refactor unlink(), create(), open(write), and write() to fail or block on virtual files in tmplink.
		});
		
		for (std::string item: items)
		{
			// 1. copy file(s)
		}
		// 2. delete and create statcache reference(s)
		// 2.1. something to make things work w/o statcache in the meantime
		// 3. unlock and delete old file(s)
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Finished background move from '" + old_bdir->path + "/" + old_relpath + "' to '" + new_bdir->path + "/" + new_relpath + "'.");
#endif
	}, old_relpath, old_bdir, new_relpath, new_bdir);
}
// </editor-fold>

// <editor-fold desc="FUSE callbacks">
static int maidfs_getattr(const char *c_path, struct stat *stbuf, struct fuse_file_info *fi)
{
	std::string path(c_path);
	int res = 0;

#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling getattr for '" + path + "'.");
#endif

	if (path == "/")
	{
		stbuf->st_mode = S_IFDIR | cfg->rootmode;
		stbuf->st_nlink = 2;
		stbuf->st_uid = cfg->rootuid;
		stbuf->st_gid = cfg->rootgid;
	}
	else
		res = getStat(path, *stbuf);
	
	return res;
}

static int maidfs_readdir(const char *c_path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi, fuse_readdir_flags frdf)
{
	std::string path(c_path);

#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling readdir for '" + path + "'.");
#endif
	
	if (cfg->stat_cache == 1 && cfg->stat_cache_prime == 1)
	{
		maidfs_log(MAIDFS_LOG_INFO, "Performing readdir(" + path + ") from stat cache.");
		
		const int sp = path == "/" ? 0 : path.size();
		
		if (sp > 0)
		{
			std::string dot(normalizePath(path + "/."));
			if (stat_cache.exists(dot))
			{
				struct stat st;
				stat_cache.get(dot, st);
				filler(buf, ".", const_cast<struct stat*>(&st), 0, FUSE_FILL_DIR_PLUS);
			}
			else
				filler(buf, ".", NULL, 0, (fuse_fill_dir_flags)0);
			std::string dotdot(normalizePath(path + "/.."));
			if (stat_cache.exists(dotdot))
			{
				struct stat st;
				stat_cache.get(dotdot, st);
				filler(buf, "..", const_cast<struct stat*>(&st), 0, FUSE_FILL_DIR_PLUS);
			}
			else
				filler(buf, "..", NULL, 0, (fuse_fill_dir_flags)0);
		}
		else
		{
			filler(buf, ".", NULL, 0, (fuse_fill_dir_flags)0);
			filler(buf, "..", NULL, 0, (fuse_fill_dir_flags)0);
		}
		
		stat_cache.filldir(path, buf, filler);
	}
	else
	{
		maidfs_log(MAIDFS_LOG_INFO, "Performing readdir(" + path + ") from backing device.");
		
		std::unordered_map<std::string, struct stat> names;
		
		for (backing *b: cfg->bdirs)
		{
			struct dirent *de;
			DIR *dh = opendir((b->path + path).c_str());
			if (!dh)
				continue;

			while (de = readdir(dh))
			{
				if (names.find(std::string(de->d_name)) != names.end())
					continue;

				std::string opath(b->path + path + de->d_name);
				struct stat st;
				lstat(opath.c_str(), &st);
				names[de->d_name] = st;
			}

			closedir(dh);
		}
		
		for (std::pair<std::string, struct stat> p: names)
			filler(buf, p.first.c_str(), const_cast<struct stat*>(&p.second), 0, (fuse_fill_dir_flags)0);
	}
	
	return 0;
}

static int maidfs_access(const char *c_path, int mode)
{
	std::string path(c_path);
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling access for '" + path + "'.");
#endif

	if (path == "/")
		return 0;
	
	if (isHidden(path))
		return -ENOENT;
	
	std::string rpath(getBackingPath(path));
	
	if (rpath.empty())
		return -ENOENT;
	
	int r = access(rpath.c_str(), mode); // BUG not enough for allow_other
	if (r == -1)
		return -errno;
	return 0;
}

static int maidfs_fallocate(const char *c_path, int mode, off_t offset, off_t length, struct fuse_file_info *fi)
{
	std::string path(c_path);
	std::string rpath(getBackingPath(path));

#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling fallocate for '" + path + "'.");
#endif

	if (reallocMoveFile(path, rpath, offset + length) == 0)
	{
		close(fi->fh);
		int fd = open(rpath.c_str(), fdflags[fi->fh].flags);
		if (fd != 0)
			return -errno;
		fdflags[fd] = fdflags[fi->fh];
		fdflags.erase(fi->fh);
		fi->fh = fd;
	}
	
	int r = fallocate(fi->fh, mode, offset, length);
	if (r != 0)
		return -errno;
		
	backing *bdev(getBacking(rpath));
	bdev->invalidateFreeSpaceCache();
	flushStat(path);
	
	return 0;
}

static int maidfs_open(const char *c_path, struct fuse_file_info *fi)
{
	std::string path(c_path);
	std::string rpath(getBackingPath(path));
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling open(" + std::to_string(fi->flags) + ") for '" + path + "'.");
#endif

	if (rpath.empty())
		return -ENOENT;
	
	int fd = open(rpath.c_str(), fi->flags);
	if (fd == -1)
		return -errno;

	if (fi->flags & O_TRUNC)
		maidfs_log(MAIDFS_LOG_WARNING, "Clearing O_TRUNC for open() for '" + path + "'.");
	
	struct fdflag fdf;
	fdf.flags = (fi->flags & ~O_TRUNC); // TODO doing this may be wrong?
	fdf.rpath = rpath;
	fdf.b = getBacking(rpath);
	
	fdflags[fd] = fdf;
	fi->fh = fd;
	
	return 0;
}

static int maidfs_create(const char *c_path, mode_t mode, struct fuse_file_info *fi)
{
	std::string path(c_path);
	std::string ppath(path.substr(0, path.find_last_of('/')));
	std::string rpath(getNewBackingPath(path));
	std::string prpath(rpath.substr(0, rpath.find_last_of('/')));
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling create/open(" + std::to_string(fi->flags) + ") for '" + path + "'.");
#endif

	if (rpath.empty())
		return -ENOSPC;
	
	struct fuse_context *fx = fuse_get_context();
	mode_t mask = _getumask();
	int md = mkdirr(prpath.c_str(), 0777 & ~mask, fx->uid, fx->gid);
	if (md != 0)
		return md;
	
	unsigned long fd = open(rpath.c_str(), fi->flags, mode);
	if (fd == -1)
		return -errno;
	
	chown(rpath.c_str(), fx->uid, fx->gid);
	
	struct fdflag fdf;
	fdf.flags = (fi->flags & ~(O_TRUNC | O_EXCL));
	fdf.rpath = rpath;
	fdf.b = getBacking(rpath);
	
	fdflags[fd] = fdf;
	fi->fh = fd;
	
	cacheStat(path);
	flushStat(ppath);
	
	return 0;
}

static int maidfs_mknod(const char *c_path, mode_t mode, dev_t rdev)
{
	std::string path(c_path);
	std::string ppath(path.substr(0, path.find_last_of('/')));
	std::string rpath(getNewBackingPath(path));
	std::string prpath(rpath.substr(0, rpath.find_last_of('/')));
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling mknod for '" + path + "'.");
#endif

	if (rpath.empty())
		return -ENOSPC;

	struct fuse_context *fx = fuse_get_context();
	mode_t mask = _getumask();
	int md = mkdirr(prpath.c_str(), 0777 & ~mask, fx->uid, fx->gid);
	if (md != 0)
		return md;
	
	int r;
	if (S_ISREG(mode))
	{
		r = open(rpath.c_str(), O_CREAT | O_EXCL | O_WRONLY, mode);
		if (r >= 0)
			r = close(r);
	}
	else if (S_ISFIFO(mode))
		r = mkfifo(rpath.c_str(), mode);
	else
		r = mknod(rpath.c_str(), mode, rdev);
	
	if (r == -1)
		return -errno;
	
	chown(rpath.c_str(), fx->uid, fx->gid);
	
	cacheStat(path);
	flushStat(ppath);
	
	return 0;
}

static int maidfs_mkdir(const char *c_path, mode_t mode)
{
	std::string path(c_path);
	std::string ppath(path.substr(0, path.find_last_of('/')));
	std::string rpath(getNewBackingPath(path));
	std::string prpath(rpath.substr(0, rpath.find_last_of('/')));
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling mkdir for '" + path + "'.");
#endif

	struct fuse_context *fx = fuse_get_context();
	mode_t mask = _getumask();
	int md = mkdirr(prpath.c_str(), 0777 & ~mask, fx->uid, fx->gid);
	if (md != 0)
		return md;
	
	int r = mkdir(rpath.c_str(), mode);
	if (r == -1)
		return -errno;
	
	if (getuid() == 0)
	{
		struct fuse_context *fx = fuse_get_context();
		gid_t gid = fx->gid;
		struct stat st;
		if (lstat(rpath.c_str(), &st) == 0)
			if (st.st_gid != getgid())
				gid = st.st_gid;
		chown(rpath.c_str(), fx->uid, gid);
	}
	
	backing *bdev(getBacking(rpath));
	bdev->invalidateFreeSpaceCache();
	cacheStat(path);
	flushStat(ppath);

	return 0;
}

static int maidfs_release(const char *c_path, struct fuse_file_info *fi)
{
	std::string path(c_path);
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling release for '" + path + "'.");
#endif

	flushStat(path);
	
	if (close(fi->fh) == -1)
		return -errno;
	
	fdflags.erase(fi->fh);
	
	return 0;
}

static int maidfs_read(const char *c_path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
	//std::string path(c_path);
	//std::string rpath(fdflags[fi->fh].rpath);
		
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling read(size: " + std::to_string(size) + ", offset: " + std::to_string(offset) + ") for '" + std::string(c_path) + "'.");
#endif

	int r = pread(fi->fh, buf, size, offset);
	
	return r;
}

static int maidfs_write(const char *c_path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
	std::string path(c_path);

#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling write(size: " + std::to_string(size) + ", offset: " + std::to_string(offset) + ") for '" + path + "'.");
#endif

	std::string rpath(fdflags[fi->fh].rpath);
	backing *b = fdflags[fi->fh].b;
	if (rpath.empty())
	{
		rpath = fdtopath(fi->fh);
		fdflags[fi->fh].rpath = rpath;
		b = getBacking(rpath);
		fdflags[fi->fh].b = b;
	}
		
	if (b->getFreeSpace() - size <= b->minfree)
	{
		if (fdflags.find(fi->fh) == fdflags.end())
		{
			maidfs_log(MAIDFS_LOG_ERROR, "Moving file " + path + " to new backing device before write failed due to missing open flags.");
			return -ENOSPC;
		}
		
		maidfs_log(MAIDFS_LOG_MSG, "Attempting to move file " + path + " to new backing device before write.");
		close(fi->fh);

		int rn = reallocMoveFile(path, rpath, size + offset);
		if (rn < 0) // 0=OK, -n=ERR, 1=notmoved; in case of 1 we need to reopen the file anyways
			return rn;

		stat_cache.repl(path, getBacking(rpath));

		int fd = open(rpath.c_str(), fdflags[fi->fh].flags); // Flags are cleaned up in maidfs_create()
		if (fd == -1)
			return -errno;

		fdflags[fd] = fdflags[fi->fh];
		fdflags.erase(fi->fh);
		fdflags[fd].rpath = rpath;
		fdflags[fd].b = getBacking(rpath);

		fi->fh = fd;
	}
	
	int r = pwrite(fi->fh, buf, size, offset);
	
	b->invalidateFreeSpaceCache(r, false);
	flushStat(path);
	
	return r;
}

static int maidfs_fsync(const char *c_path, int datasync, struct fuse_file_info *fi)
{
	int r;
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling fsync(datasync: " + std::to_string(datasync) + ") for '" + std::string(c_path) + "'.");
#endif

	if (datasync)
		r = fdatasync(fi->fh);
	else
		r = fsync(fi->fh);
	
	if (r == -1)
		return -errno;
	
	return 0;
}

static int maidfs_truncate(const char *c_path, off_t size, struct fuse_file_info *fi)
{
	std::string path(c_path);
	std::string rpath(getBackingPath(path));
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling truncate(size: " + std::to_string(size) + ") for '" + path + "'.");
#endif

	if (rpath.empty()) // this is correct as per man truncate(2)!
		return -ENOENT;

	backing *b = getBacking(rpath);
	if (b->getFreeSpace() - size <= b->minfree)
	{
		maidfs_log(MAIDFS_LOG_MSG, "Attempting to move file " + path + " to new backing device before truncate.");
		int rn = reallocMoveFile(path, rpath, size);
		if (rn < 0)
			return rn;
		stat_cache.repl(path, getBacking(rpath));
	}

	int r = truncate(rpath.c_str(), size);
	if (r == -1)
		return -errno;
	
	backing *bdev(getBacking(rpath));
	bdev->invalidateFreeSpaceCache();
	flushStat(path);
	
	return 0;
}

static int maidfs_ftruncate(const char *c_path, off_t size, struct fuse_file_info *fi)
{
	std::string path(c_path);
	std::string rpath(getBackingPath(path));
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling ftruncate(size: " + std::to_string(size) + ") for '" + path + "'.");
#endif

	if (rpath.empty())
		return -ENOENT;

	backing *b = getBacking(rpath);
	if (b->getFreeSpace() - size <= b->minfree)
	{
		maidfs_log(MAIDFS_LOG_MSG, "Attempting to move file " + path + " to new backing device before truncate.");
		close(fi->fh);

		int rn = reallocMoveFile(path, rpath, size);
		if (rn < 0)
			return rn;
		backing *nb = getBacking(rpath);
		stat_cache.repl(path, nb);

		int fd = open(rpath.c_str(), fdflags[fi->fh].flags);
		if (fd == -1)
			return -errno;

		fdflags[fd] = fdflags[fi->fh];
		fdflags.erase(fi->fh);
		fdflags[fd].rpath = rpath;
		fdflags[fd].b = nb;

		fi->fh = fd;
	}

	int r = ftruncate(fi->fh, size);
	if (r == -1)
		return -errno;

	backing *bdev(getBacking(rpath));
	bdev->invalidateFreeSpaceCache();
	flushStat(path);
	
	return 0;
}

static int maidfs_opendir(const char *path, struct fuse_file_info *fi)
{
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling opendir (noop) for '" + std::string(path) + "'.");
#endif

	return 0;
}

static int maidfs_readlink(const char *c_path, char *buf, size_t size)
{
	std::string path(c_path);
	std::string rpath(getBackingPath(path));
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling readlink for '" + path + "'.");
#endif

	if (rpath.empty())
		return -1;
	
	int r = readlink(rpath.c_str(), buf, size);
	if (r >= 0)
		return 0;
	return -1;
}

static int maidfs_unlink(const char *c_path)
{
	std::string path(c_path);
	std::string ppath(path.substr(0, path.find_last_of('/')));
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling unlink for '" + path + "'.");
#endif

	if (cfg->multibacking == 1)
	{
		std::vector<std::string> rpath = getBackingPathAll(path);
		if (rpath.empty())
			return -ENOENT;
		for (std::string rp: rpath)
		{
			int r = unlink(rp.c_str());
			if (r == -1)
				return -errno;
		}
	}
	else
	{
		std::string rpath(getBackingPath(path));
		if (rpath.empty())
			return -ENOENT;
		
		int r = unlink(rpath.c_str());

		if (r == -1)
			return -errno;

		backing *bdev(getBacking(rpath));
		bdev->invalidateFreeSpaceCache();
	}
	
	deleteStat(path);
	flushStat(ppath);
	
	return 0;
}

static int maidfs_rmdir(const char *c_path)
{
	std::string path(c_path);
	std::string ppath(path.substr(0, path.find_last_of('/')));

#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling rmdir for '" + path + "'.");
#endif

	if (cfg->multibacking == 1)
	{
		std::vector<std::string> rpath = getBackingPathAll(path);
		
		if (rpath.empty())
			return -ENOENT;
		
		for (std::string rp: rpath)
		{
			int r = rmdir(rp.c_str());
			if (r == -1)
				return -errno;
		}
	}
	else
	{
		std::string rpath(getBackingPath(path));

		if (rpath.empty())
			return -ENOENT;

		int r = rmdir(rpath.c_str());

		if (r == -1)
			return -errno;

		backing *bdev(getBacking(rpath));
		bdev->invalidateFreeSpaceCache();
	}
	
	deleteStat(path);
	flushStat(ppath);
	
	return 0;
}

static int maidfs_symlink(const char *c_path_old, const char *c_path_new)
{
	std::string path_old(c_path_old);
	std::string path_new(c_path_new);
	std::string rpath_new(getNewBackingPath(path_new));
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling symlink for '" + path_old + "' => '" + path_new + "'.");
#endif

	if (rpath_new.empty())
		return -ENOSPC;
	
	int r = symlink(c_path_old, rpath_new.c_str());
	if (r == -1)
		return -errno;
	
	cacheStat(path_new);
	
	return 0;
}

static int maidfs_rename(const char *c_path_old, const char *c_path_new, unsigned int flags)
{
	if (flags & (RENAME_EXCHANGE | RENAME_WHITEOUT) > 0)
		return -EINVAL;
	
	// rename is also move
	std::string path_old(c_path_old);
	std::string path_new(c_path_new);
	int ret = 0;

#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling rename for '" + path_old + "' => '" + path_new + "'.");
#endif
	
	if (flags & RENAME_NOREPLACE > 0)
	{
		if (getBackingPath(path_new) != "")
			return -EEXIST;
	}
	
	if (cfg->multibacking == 1)
	{
		std::vector<std::string> rpath_old = getBackingPathAll(path_old);
		
		if (rpath_old.empty())
			return -ENOENT;
		
		for (std::string rpo: rpath_old)
		{
			if (rpo.empty())
				return -ENOENT;
			
			std::string rpath_new(getBacking(rpo)->path + path_new);
			
			struct fuse_context *fx = fuse_get_context();
			mode_t mask = _getumask();
			int md = mkdirr(rpath_new.substr(0, rpath_new.find_last_of('/')).c_str(), 0777 & ~mask, fx->uid, fx->gid);
			if (md != 0)
				return md;
			
			int r = rename(rpo.c_str(), rpath_new.c_str());
			if (r == -1)
				return -errno;
		}
	}
	else
	{
		std::string rpath_old(getBackingPath(path_old));

		if (rpath_old.empty())
			return -ENOENT; // nothing happened yet, no need to finalize
		
		std::string rpath_new;
		struct stat st;
		
		if (cfg->rename_realloc > 0)
		{
			getStat(path_old, st);
			rpath_new = getNewBackingPathSize(
				path_new,
				cfg->rename_realloc == 2 ? 0 : st.st_size);
		}
		else
			rpath_new = normalizePath(getBacking(rpath_old)->path + path_new);

		if (rpath_new.empty())
			return -ENOSPC; // nothing happened yet, no need to finalize

		struct fuse_context *fx = fuse_get_context();
		mode_t mask = _getumask();
		int md = mkdirr(rpath_new.substr(0, rpath_new.find_last_of('/')).c_str(), 0777 & ~mask, fx->uid, fx->gid);
		if (md != 0)
		{
			ret = md;
			goto finalize;
		}

		int r;
		if (getBacking(rpath_old) == getBacking(rpath_new))
		{
#ifdef DEBUG
			maidfs_log(MAIDFS_LOG_DEBUG, "Doing rename by renaming: " + rpath_old + " => " + rpath_new);
#endif
			r = rename(rpath_old.c_str(), rpath_new.c_str());
		}
		else
		{
#ifdef DEBUG
			maidfs_log(MAIDFS_LOG_DEBUG, "Doing rename by invoking mv " + rpath_old + " " + rpath_new);
#endif
			r = move(rpath_old, rpath_new);

			backing *bdev(getBacking(rpath_old));
			bdev->invalidateFreeSpaceCache();
			bdev = getBacking(rpath_new);
			bdev->invalidateFreeSpaceCache();
		}

		if (r == -1)
		{
			ret = -errno;
			goto finalize;
		}
	}
	
finalize:
	
	flushStat(path_old, true);
	cacheStat(path_new, true);
	flushStat(path_new, true);
	flushStat(path_new.substr(0, path_new.find_last_of('/')));
	
	return ret;
}

static int maidfs_link(const char *c_path_old, const char *c_path_new)
{
	std::string path_old(c_path_old);
	std::string rpath_old(getBackingPath(path_old));
	std::string path_new(c_path_new);
	std::string rpath_new(rpath_old.substr(0, rpath_old.find_last_of('/')) + "/" + path_new);
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling link for '" + path_old + "' => '" + path_new + "'.");
#endif

	if (rpath_old.empty())
		return -ENOENT;
	
	int r = link(rpath_old.c_str(), rpath_new.c_str());
	if (r == -1)
		return -errno;
	
	cacheStat(path_new);
	
	return 0;
}

static int maidfs_chmod(const char *c_path, mode_t mode, struct fuse_file_info *fi)
{
	std::string path(c_path);
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling chmod for '" + path + "'.");
#endif

	struct stat st;
	getStat(path, st);
	bool isDir = S_ISDIR(st.st_mode);

	if (isDir || cfg->multibacking == 1)
	{
		std::vector<std::string> rpath = getBackingPathAll(path);
		
		if (rpath.empty())
			return -ENOENT;
		
		for (std::string rp: rpath)
		{
			if (rp.empty())
				return -ENOENT;
		
#ifdef DEBUG
			maidfs_log(MAIDFS_LOG_DEBUG, "Multibacking chmod for '" + rp + "'");
#endif
			
			int r = chmod(rp.c_str(), mode);
			
			if (r == -1)
				return -errno;
		}
	}
	else
	{
		std::string rpath(getBackingPath(path));

		if (rpath.empty())
			return -ENOENT;

		int r = chmod(rpath.c_str(), mode);

		if (r == -1)
			return -errno;
	}
	
	flushStat(path);
	
	return 0;
}

static int maidfs_chown(const char *c_path, uid_t uid, gid_t gid, struct fuse_file_info *fi)
{
	std::string path(c_path);
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling chown for '" + path + "'.");
#endif

	struct stat st;
	getStat(path, st);
	bool isDir = S_ISDIR(st.st_mode);

	if (isDir || cfg->multibacking == 1)
	{
		std::vector<std::string> rpath = getBackingPathAll(path);
		
		if (rpath.empty())
			return -ENOENT;
		
		for (std::string rp: rpath)
		{
#ifdef DEBUG
			maidfs_log(MAIDFS_LOG_DEBUG, "Multibacking lchown for '" + rp + "'");
#endif
			int r = lchown(rp.c_str(), uid, gid);
			
			if (r == -1)
				return -errno;
		}
	}
	else
	{
		std::string rpath(getBackingPath(path));

		if (rpath.empty())
			return -ENOENT;

		int r = lchown(rpath.c_str(), uid, gid);

		if (r == -1)
			return -errno;
	}
	
	flushStat(path);
	
	return 0;
}

static int maidfs_utimens(const char *c_path, const struct timespec ts[2], struct fuse_file_info *fi)
{
	std::string path(c_path);
	
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling utimens for '" + path + "'.");
#endif

	struct timeval tv[2];
	tv[0] = timespectotimeval(ts[0]);
	tv[1] = timespectotimeval(ts[1]);
	
	if (cfg->multibacking == 1)
	{
		std::vector<std::string> rpath = getBackingPathAll(path);
		
		if (rpath.empty())
			return -ENOENT;
		
		for (std::string rp: rpath)
		{
			if (ts[0].tv_nsec == UTIME_OMIT || ts[1].tv_nsec == UTIME_OMIT)
			{
				struct stat st;
				getStat(path, st);

				if (ts[0].tv_nsec == UTIME_OMIT)
					tv[0] = timespectotimeval(st.st_atim);

				if (ts[1].tv_nsec == UTIME_OMIT)
					tv[1] = timespectotimeval(st.st_mtim);
			}
			
			if (rp.empty())
				return -ENOENT;
			
			int r = lutimes(rp.c_str(), tv);
			
			if (r == -1)
				return -errno;
		}
	}
	else
	{
		std::string rpath(getBackingPath(path));

		if (ts[0].tv_nsec == UTIME_OMIT || ts[1].tv_nsec == UTIME_OMIT)
		{
			struct stat st;
			getStat(path, st);
		
			if (ts[0].tv_nsec == UTIME_OMIT)
				tv[0] = timespectotimeval(st.st_atim);

			if (ts[1].tv_nsec == UTIME_OMIT)
				tv[1] = timespectotimeval(st.st_mtim);
		}
		
		if (rpath.empty())
			return -ENOENT;
		
		int r = lutimes(rpath.c_str(), tv);

		if (r == -1)
			return -errno;
	}
	
	flushStat(path);
	
	return 0;
}

static int maidfs_statfs(const char *path, struct statvfs *fs)
{
#ifdef DEBUG
	maidfs_log(MAIDFS_LOG_DEBUG, "Handling statfs for '" + std::string(path) + "'.");
#endif

	fs->f_bsize = 4096;
	fs->f_frsize = 4096;
	fs->f_blocks = 0;
	fs->f_namemax = 9999999;
	
	// The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
	for (backing *b: cfg->bdirs)
	{
		struct statvfs bfs;
		if (statvfs(b->path.c_str(), &bfs) == -1)
			continue;
		double blockratio = (bfs.f_bsize / (double)fs->f_bsize);
		fs->f_blocks += blockratio * bfs.f_blocks;
		fs->f_bfree  += blockratio * bfs.f_bfree;
		fs->f_bavail += blockratio * bfs.f_bavail;
		fs->f_files  += bfs.f_files;
		fs->f_ffree  += bfs.f_ffree;
		if (bfs.f_namemax < fs->f_namemax)
			fs->f_namemax = bfs.f_namemax;
	}
	
	return 0;
}

static void maidfs_manage_command(std::string msg, struct handle_arg *ha)
{
	maidfs_log(MAIDFS_LOG_MSG, "Received management command: " + msg);
	
	std::vector<std::string> args = strsplit(msg, ' ', '\\');
	bool persistent = (args[0] == "-p");
	if (persistent)
		args.erase(std::next(args.begin(), 0));
	
	std::string pass;
	if (args[0].substr(0, 2) == "-P")
	{
		pass = args[0].substr(2, std::string::npos);
		args.erase(std::next(args.begin(), 0));
	}
	if (!cfg->manage_pass.empty() && pass != cfg->manage_pass)
	{
		args.clear();
		persistent = false;
	}
	
	if (args.size() >= 2 && (args[0] == "getbackingpath" || args[0] == "gbp"))
	{
		for (int i = 1; i < args.size(); i++)
		{
			std::string rpath(getBackingPath(args[i]) + "\n");
			send(ha->cfd, rpath.c_str(), rpath.size(), 0);
		}
	}
	else if (args.size() >= 2 && (args[0] == "getbackingpathall" || args[0] == "gbpa"))
	{
		for (int i = 1; i < args.size(); i++)
		{
			std::vector<std::string> rpath = getBackingPathAll(args[i]);
			
			for (std::string rp: rpath)
			{
				rp += "\n";
				send(ha->cfd, rp.c_str(), rp.size(), 0);
			}
		}
	}
	else if (args.size() == 3 && args[0] == "move")
	{
		struct stat sta;
		
		if (lstat(args[1].c_str(), &sta) != 0)
		{
			send(ha->cfd, "Could not stat old file.\n", 25, 0);
			goto maidfs_manage_command_close;
		}
		
		int mr = move(args[1], args[2]);
		if (mr == 0)
			// TODO flush cache? anything else?
			send(ha->cfd, "0\n", 2, 0);
		else
		{
			std::string err(std::to_string(mr) + "\n");
			send(ha->cfd, err.c_str(), err.size(), 0);
		}
	}
	else if (args.size() == 3 && (args[0] == "movetobacking" || args[0] == "mtb"))
	{
		// args[1] := file, args[2] := new backing device (by mountpoint)
		struct stat sta;

		std::string path(normalizePath(args[1]));
		std::string rpath(getBackingPath(path));
		if (lstat(rpath.c_str(), &sta) != 0)
		{
			send(ha->cfd, "Could not stat file.\n", 21, 0);
			goto maidfs_manage_command_close;
		}

		std::string argbn(normalizePath(args[2]));
		backing *bdir = nullptr;
		for (backing *b: cfg->bdirs)
		{
			if (b->path == argbn)
				bdir = b;
		}
		if (bdir == nullptr)
		{
			send(ha->cfd, "Could not find backing.", 23, 0);
			goto maidfs_manage_command_close;
		}

		std::string nrpath(bdir->path + path);

		int mr = move(rpath, nrpath);
		if (mr == 0)
		{
			flushStat(path);
			send(ha->cfd, "0\n", 2, 0);
		}
		else
		{
			std::string err(std::to_string(mr) + "\n");
			send(ha->cfd, err.c_str(), err.size(), 0);
		}
	}
	else if (args.size() >= 2 && args[0] == "flushcache")
	{
		flushStat(args[1], true);
		
		std::string buf("done");
		send(ha->cfd, buf.c_str(), buf.size(), 0);
	}
	else if (args.size() >= 1 && args[0] == "stats")
	{
		std::string buf;
		
		buf = "open_files=" + std::to_string(fdflags.size()) + "\n";
		send(ha->cfd, buf.c_str(), buf.size(), 0);

		long freespace = 0;
		for (backing *b: cfg->bdirs)
		{
			buf = "free_space[" + b->path + "]=";
			if (b->sealed > 0)
				buf += "-1(sealed)";
			else
			{
				freespace = b->getFreeSpace();
				buf += std::to_string(freespace);
				if (freespace < b->minfree)
					buf += "*";
			}
			buf += "\n";
			send(ha->cfd, buf.c_str(), buf.size(), 0);
		}

/*		buf = "stat_cache_count=" + std::to_string(stat_cache.size()) + "\n";
		send(ha->cfd, buf.c_str(), buf.size(), 0);
		buf = "stat_cache_size=" + std::to_string(
				(unsigned long)(stat_cache.max_load_factor() > 1.0
				? stat_cache.bucket_count() * stat_cache.max_load_factor()
				: stat_cache.bucket_count())
			) + "\n";
		send(ha->cfd, buf.c_str(), buf.size(), 0);
		buf = "stat_cache_hit=" + std::to_string(stat_cache_hit) + "\n";
		send(ha->cfd, buf.c_str(), buf.size(), 0);
		buf = "stat_cache_miss=" + std::to_string(stat_cache_miss) + "\n";
		send(ha->cfd, buf.c_str(), buf.size(), 0);
		buf = "stat_cache_del=" + std::to_string(stat_cache_del) + "\n";
		send(ha->cfd, buf.c_str(), buf.size(), 0); */
	}
	else if (args.size() >= 2 && args[0] == "setloglevel")
	{
		int newlevel = atoi(args[1].c_str());
		cfg->log_level = newlevel;
		std::string buf("cfg->log_level=" + std::to_string(newlevel) + "\n");
		send(ha->cfd, buf.c_str(), buf.size(), 0);
	}
	else
		send(ha->cfd, "Unknown command.\n", 17, 0);

maidfs_manage_command_close:
	if (!persistent || args[0] == "quit")
	{
		shutdown(ha->cfd, SHUT_RDWR);
		close(ha->cfd);
	}
}

static void* maidfs_init(struct fuse_conn_info *fconn, struct fuse_config *fcfg)
{
	command_queue = new CommandQueue();
	
	if (cfg->manage_port > 0)
	{
		maidfs_log(MAIDFS_LOG_MSG, "Manage port: " + std::to_string(cfg->manage_port), true);
		mansrv = new tcpserver(cfg->manage_port, maidfs_manage_command);
	}

	fcfg->use_ino = 1;
	
	return nullptr;
}

static void maidfs_destroy(void *private_data)
{
	if (cfg->manage_port > 0)
	{
#ifdef DEBUG
		maidfs_log(MAIDFS_LOG_DEBUG, "Stopping management interface server thread.");
#endif
		mansrv->active = false;
	}
	
	if (fdflags.size() > 0)
		maidfs_log(MAIDFS_LOG_WARNING, "There are " + std::to_string(fdflags.size()) + " uncleared fdflags (open files?).");
	fdflags.clear();

	log_m.lock();
	logStream.close();
	log_m.unlock();
}

struct maidfs_fuse_operations : fuse_operations
{
	maidfs_fuse_operations()
	{
		this->getattr = maidfs_getattr;
		this->readlink = maidfs_readlink;
		this->mknod = maidfs_mknod;
		this->mkdir = maidfs_mkdir;
		this->unlink = maidfs_unlink;
		this->rmdir = maidfs_rmdir;
		this->symlink = maidfs_symlink;
		this->rename = maidfs_rename;
		this->link = maidfs_link;
		this->chmod = maidfs_chmod;
		this->chown = maidfs_chown;
		this->truncate = maidfs_truncate;
		this->open = maidfs_open;
		this->read = maidfs_read;
		this->write = maidfs_write;
		this->statfs = maidfs_statfs;
		// flush
		this->release = maidfs_release;
		this->fsync = maidfs_fsync;
		// setxattr
		// getxattr
		// listxattr
		// removexattr
		this->opendir = maidfs_opendir;
		this->readdir = maidfs_readdir;
		// releasedir
		// fsyncdir
		this->access = maidfs_access;
		this->create = maidfs_create;
		//this->ftruncate = maidfs_ftruncate;
		// fgetattr
		// lock
		this->utimens = maidfs_utimens;
		// bmap
		// ioctl
		// poll
		// write_buf
		// read_buf
		// flock
		this->fallocate = maidfs_fallocate;
		this->init = maidfs_init;
		this->destroy = maidfs_destroy;
	}
};

static struct maidfs_fuse_operations maidfs_oper;
// </editor-fold>

int main(int argc, char** argv)
{
	std::cout << "maidfs " << MAIDFS_VERSION << " " << MAIDFS_GIT_REF;
#ifdef DEBUG
	std::cout << " debug";
#endif
	std::cout << std::endl;
	
	cfg = new config(argv[1]);
	
	if (!cfg->log.empty())
		logStream.open(cfg->log, std::ios_base::app);
	
	for (backing *b: cfg->bdirs)
	{
		maidfs_log(MAIDFS_LOG_MSG, "Backing: " + b->path, true);
		
		struct stat st;
		int sr = lstat(b->path.c_str(), &st);
		
		if (!S_ISDIR(st.st_mode))
		{
			maidfs_log(MAIDFS_LOG_ERROR, "Backing path " + b->path + " not found.", true);
			return ENOENT;
		}
		
		if (b->allow_empty != 1 && dir_is_empty(b->path))
		{
			maidfs_log(MAIDFS_LOG_ERROR, "Backing path " + b->path + " is empty. (Set allow_empty=1 for this backing device if this is intentional)", true);
			return ENOENT;
		}
	}
	
	if (cfg->stat_cache == 1 && cfg->stat_cache_prime == 1)
		primeStatCache();
	
	for (int i = 1; i < argc; i++)
		argv[i] = argv[i + 1];
	argc--;
	
	return fuse_main_real(argc, argv, &maidfs_oper, sizeof(fuse_operations), NULL);
}


#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <vector>

#include <sys/types.h>

#include "backing.h"

class config {
public:
	std::vector<backing*> bdirs;
	char stat_cache;
	char stat_cache_prime;
	std::string log;
	char log_level;
	int manage_port;
	std::string manage_pass;
	std::string name;
	char multibacking;
	char rename_realloc;
	char preferexisting;
	mode_t rootmode;
	uid_t rootuid;
	gid_t rootgid;
	std::vector<std::string> hidden;
	
	config(std::string path);
	virtual ~config();
private:
	void parse(std::string path);
};

#endif /* CONFIG_H */


#ifndef BACKING_H
#define BACKING_H

#include <string>
#include <vector>
#include <mutex>

#include <sys/time.h>

class backing
{
public:
	std::string path;
	std::string dev;
	std::vector<std::string> preferred_dirs;
	ulong minfree;
	char allow_empty;
	char sealed;
	unsigned short fsc_enable;
	
	backing(std::string path);
	backing(const backing& orig);
	
	unsigned long getFreeSpace();
	void invalidateFreeSpaceCache(int changeSizeHint = 0, bool force = true);
	
	virtual ~backing();
private:
	std::mutex fsc_m;
	volatile unsigned long fsc_v;
	volatile unsigned long fsc_t;
};

#endif /* BACKING_H */


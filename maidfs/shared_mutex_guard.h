#ifndef SHARED_LOCK_GUARD_H
#define SHARED_LOCK_GUARD_H

#include <shared_mutex>

class shared_mutex_guard
{
private:
    std::shared_mutex& m;

public:
    explicit shared_mutex_guard(std::shared_mutex& m_):
        m(m_)
    {
        m.lock_shared();
    }

    ~shared_mutex_guard()
    {
        m.unlock_shared();
    }
};

#endif /*SHARED_LOCK_GUARD_H*/
#ifndef CVECTOR_H
#define CVECTOR_H

#include <utility>
#include <vector>
#include <shared_mutex>

#include "shared_mutex_guard.h"

template <typename T>
class cvector
{
private:
	std::vector<T> v;
	std::shared_mutex m;
	
public:
	
	cvector()
	{
		
	}
	
	cvector(const std::vector<T>& vec)
	{
		std::lock_guard<std::shared_mutex> lg(this->m);
		for (T i: vec)
			this->v.push_back(i);
	}
	
	cvector& operator= (const std::vector<T>& vec)
	{
		this->v = vec;
		return *this;
	}
	
	void push_back(const T& item)
	{
		std::lock_guard<std::shared_mutex> lg(this->m);
		this->v.push_back(item);
	}
	
	typename std::vector<T>::iterator erase(typename std::vector<T>::const_iterator position)
	{
		std::lock_guard<std::shared_mutex> lg(this->m);
		return this->v.erase(position);
	}
	
	T& operator[] (size_t pos)
	{
		shared_mutex_guard lg(this->m);
		return this->v[pos];
	}
	
	T& front()
	{
		shared_mutex_guard lg(this->m);
		return this->v.front();
	}
	
	T& back()
	{
		shared_mutex_guard lg(this->m);
		return this->v.back();
	}
	
	void pop_back()
	{
		std::lock_guard<std::shared_mutex> lg(this->m);
		return this->v.pop_back();
	}
	
	typename std::vector<T>::iterator begin()
	{
		shared_mutex_guard lg(this->m);
		return this->v.begin();
	}
	
	typename std::vector<T>::const_iterator cbegin()
	{
		shared_mutex_guard lg(this->m);
		return this->v.cbegin();
	}
	
	typename std::vector<T>::iterator end()
	{
		shared_mutex_guard lg(this->m);
		return this->v.end();
	}
	
	typename std::vector<T>::const_iterator cend()
	{
		shared_mutex_guard lg(this->m);
		return this->v.cend();
	}
	
	bool empty()
	{
		shared_mutex_guard lg(this->m);
		return this->v.empty();
	}
	
	long unsigned int size()
	{
		shared_mutex_guard lg(this->m);
		return this->v.size();
	}
};

#endif /* CVECTOR_H */


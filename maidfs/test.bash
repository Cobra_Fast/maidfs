#!/bin/bash

BINARY=dist/Debug/GNU-Linux/maidfs
MOUNTPOINT=/home/cobra/pkg/maidfs/maidfs/mountpoint

stat $BINARY
sudo $BINARY test.config -o allow_other $MOUNTPOINT || exit 1
bats test.bats
sudo fusermount -u $MOUNTPOINT || exit 2

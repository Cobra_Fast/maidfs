# maidfs

## Configuration

The configuration format follows a relatively simple INI-like syntax consisting of one key-value-pairs (i.e. `key=value`) per line divided into blocks by a header (i.e. `[block]`).

Blocks can always be repeated.

Repetition of keys within the same block either overrides a previous setting or adds another value to those settings that accept multiple values.

An example configuration file [`example.cfg`](https://gitlab.com/Cobra_Fast/maidfs/blob/master/maidfs/example.cfg) can be found among the other files in the source code repository.

### General

The header is `[general]`.

| Name | Default Value | Description |
|-|-|-|
| `name` | (empty) | Name of the maidfs instance. Appears in logs. |
| `manage_port` | `0` | TCP/IP port for the management interface (e.g. for use with `maidfsm`) |
| `log` | (empty) | Path to the log output file. |
| `log_level` | `0` | Only messages with a lower (!) level will be written to file. |
| `stat_cache` | `0` | Set to `1` to enable stat object caching functionality. |
| `stat_cache_prime` | `0` | Set to `1` to enable stat cache priming. This will constantly keep all possible stat objects cached and additionally perform directory reads (listing) from the cache. |
| `rename_realloc` | `0` | Set to `1` to enable moving files between backing directories when moving within maidfs. Will follow allocation rules similarly to if a new file was going to be saved including the file's size. Set to `2` to do the same but ignore the file's size. |
| `multibacking` | `0` | Experimental/Incomplete. Allow files to be in several backing directories and apply changes to all copies. |
| `rootmode` | `0755` | Access modes for the mount point. |
| `rootuid` | `0` | User ID for the mount point. |
| `rootgid` | `0` | Group ID for the mount point. |
| `preferexisting` | `0` | Set to `1` to prefer existing directories for new files if no `prefer_dir` rule applies. |
| `hide` | (empty) | Specify a (virtual) prefix (e.g. `/.snapshot`) that should be hidden within maidfs. Hides by absolute prefix, so adding a trailing slash is recommended to avoid false negatives but will leave a ghost directory. Hiding is also a weak mechanic that does not deny reading or writing from hidden files per se.<br>Can be repeated to hide multiple prefixes. |

### Backing

The header is `[backing:path]` where `path` is a filesystem directory path to the desired directory (e.g. `/mnt/data1`).

| Name | Default Value | Description |
|-|-|-|
| `dev` | (empty) | Unused. Indicate phyiscal device in `/dev`. |
| `minfree` | `0` | Space in bytes to keep unused (free) in the backing filesystem. |
| `prefer_dir` | (empty) | Specify a (virtual) directory (e.g. `/music`) that should be kept on this backing filesystem.<br>Can be repeated to prefer multiple directories. |
| `allow_empty` | `0` | Allow maidfs to start if this backing device appears empty. Empty directories often indicate failed mounts. |
| `fsc_enable` | `0` | Enable free space cache to reduce the amount of `statvfs` calls to the underlying filesystem. Set `0` to disable, `1` to cache indefinitely, or a larger number to indicate the cache lifetime in seconds. |
| `sealed` | `0` | Makes free space checks always return `-1` bytes of available storage, avoiding files to be created or moved there. |

#### Directory preferrence

When adding a new file, maidfs will first check all drives for applicable `prefer_dir` rules in the same order as they're listed in the config file. The first drive with an applicable rule and enough free space will be used to store the new file. The space check may not be available for all operations that create new files (e.g. downloads that start out small, but a `mv` within maidfs will).

If no drive was identified via `prefer_dir`, `preferexisting` is considered, if set inspecting all drives (again, in configured order) for an existing parent directory to store the file in. Again, an available space check is performed if possible. Any level of parent directory is considered valid (e.g. `/foo/` will match to store `/foo/bar/baz/derp.zip`).

At last, the first drive (again, in configured order) with enough free space will be used.

### Cache

This feature has been removed from the master branch, build the very outdated `simplecache` branch to use it (don't). But I strongly recommend you use [`bcache`](https://bcache.evilpiepirate.org/) for your caching needs instead, as it offers higher performance and less overhead than any FUSE caching implementation.

The header is `[cache:path]` where `path` is a filesystem directory path to the desired directory (e.g. `/mnt/cache`).

Important: The cache filesystem needs to fully support access times in order for the replacement method `lru` to work properly. Use mount option `strictatime`, `relatime` might also work with limitations, `noatime` will not work properly.

| Name | Default Value | Description |
|-|-|-|
| `dev` | (empty) | Unused. |
| `method` | `rand` | Method for removing old items to make space for new ones.<br>Supported methods are: `rand` (random selection), `lru` (least recently used), and `fifo` (item longest in the cache will be removed). |
| `limit_mode` | `none` | Mode for applying the `file_size_min` and `file_size_max` setting.<br>Valid modes are: `none` (limits ignored), `nocache` (files outside of limits will not be cached), `head` (only the first bytes up to `file_size_max` will be cached). |
| `file_size_min` | `0` | Minimum size in bytes for a file to be eligible for caching. |
| `file_size_max` | `0` | Maximum size in bytes for a file to be eligible for caching. A value of zero (0) means no limit. |
| `read_intent_ratio` | `0` | Only applies when using `limit_mode=head`. Specifies a fraction (e.g. `0.5`) of `file_size_max` when a `posix_fadvise(WILLNEED)` call to the file on backing will be issued, telling the kernel to start caching file contents ahead of time. Useful for spinning up sleeping drives early. |
| `minfree` | `0` | Space in bytes to keep unused (free) in the cache filesystem. |
| `exclude_dir` | (empty) | Specify (virtual) directories (e.g. '/documents') to exclude from caching in this cache.<br>Can be repeated to exclude multiple directories. |

## Management Commands

Use the `maidfsm` bash script tool to call these slightly comfortably.

Tool invokation format: `maidfsm <command> [args...]`

**`getbackingpath <path>`**

Alias: `gbp`  
Get the absolute backing path for `<path>`.

**`getbackingpathall <path>`**

Alias: `gbpa`  
Get all absolute backing paths for `<path>`. Independant from the `multibacking` option.

**`move <oldpath> <newpath>`**

Experimental/Incomplete. Move a file to a different directory.

**`movetobacking <oldpath> <newbackingpath>`**

Alias: `mtb`  
Move file to other backing device.  
`<oldpath>` should be a maidfs-absolute path to the file.
`<newbackingpath>` should be the same as defined in the backing configuration.
If old and new backing path are identical something bad may or may not happen.

Example: `mtb /stuff/file.txt /mnt/hdd2`

**`flushcache <path>`**

Invalidate all caches (content and stat) for `<path>`. Honors `stat_cache_prime`.

**`stats`**

Fetch some internal statistics.

**`setloglevel <level>`**

Set log level (numeric).

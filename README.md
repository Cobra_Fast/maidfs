# maidfs

Massive array of idle disks file system, similar to mhddfs but more features and control.

## Main Features

* Merge multiple directory structures into one.
* Move-on-Write when a backing device nears maximum (configured) capacity.
* Comprehensive caching of directory structure. `ls` and `find` fully from cache, without waking up any drives.
* Configurable "what goes where"
* Configurable directory hiding
* Interactive (restartless) management (WIP)

---
